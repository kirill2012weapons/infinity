<?php

namespace App\EventListener;

use App\Entity\Option\AboutUs\AboutUsPart;
use App\Service\Uploader\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class OptionAboutUsUploaderListener
{
    private $uploader;

    public function __construct(File $uploader)
    {
        $this->uploader = $uploader;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof AboutUsPart) {
            return;
        }

        if ($fileName = $entity->getImage()) {
            $entity->setImage(new \Symfony\Component\HttpFoundation\File\File($this->uploader->getTargetDirectory().'/'.$fileName));
        }
    }

    private function uploadFile($entity)
    {
        if (!$entity instanceof AboutUsPart) {
            return;
        }

        $file = $entity->getImage();

        if ($file instanceof UploadedFile) {
            $fileName = $this->uploader->upload($file);
            $entity->setImage($fileName);
        }

        if ($file instanceof \Symfony\Component\HttpFoundation\File\File) {
            $entity->setImage($file->getFilename());
        }
    }
}