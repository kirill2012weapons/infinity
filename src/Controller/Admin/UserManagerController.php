<?php

namespace App\Controller\Admin;

use App\Entity\Auth\User;
use App\Forms\Auth\FinityUserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

/**
 * @Route("admin/user", name="admin_user_")
 */
class UserManagerController extends AbstractController
{

    /**
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/finity", name="finity_index")
     */
    public function index(EntityManagerInterface $entityManager)
    {
        return $this->render('admin/controllers/user_manager/index.html.twig', [
            'users' => $entityManager->getRepository(User::class)->findByRole('ROLE_USER_INFINITY')
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param EncoderFactoryInterface $encoderFactory
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @Route("/finity/create", name="finity_create")
     */
    public function finityCreate(
        Request $request,
        EntityManagerInterface $entityManager,
        EncoderFactoryInterface $encoderFactory,
        Session $session
    )
    {
        $user    = new User();
        $encoder = $encoderFactory->getEncoder($user);

        $user->setPassword($encoder->encodePassword('finityx', ''));
        $user->setPlainPassword('finityx');
        $user->addRole('ROLE_USER_INFINITY');

        $userForm = $this->createForm(
            FinityUserType::class,
            $user
        )->handleRequest($request);

        if ($user->getUsername() && $entityManager->getRepository(User::class)->isExistUserName($user->getUsername())) {
            $session->getFlashBag()->add('error', 'User with username - ' . $user->getUsername() . ' is exist');
            return $this->redirectToRoute('admin_user_finity_create');
        }

        if ($user->getEmail() && $entityManager->getRepository(User::class)->isExistEmail($user->getEmail())) {
            $session->getFlashBag()->add('error', 'User with email - ' . $user->getEmail() . ' is exist');
            return $this->redirectToRoute('admin_user_finity_create');
        }

        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('admin_user_finity_index');
        }

        return $this->render('admin/controllers/user_manager/finity_user_create.html.twig', [
            'userForm' => $userForm->createView(),
        ]);
    }

    /**
     * @param User $user
     * @param Session $session
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @Route("/{id}/edit", name="finity_edit")
     */
    public function finityEdit(User $user, Session $session, Request $request, EntityManagerInterface $entityManager)
    {
        if (!$user->hasRole('ROLE_USER_INFINITY')) {
            $session->getFlashBag()->add('error', 'User has not finity role - ' . $user->getUsername());
            return $this->redirectToRoute('admin_user_finity_index');
        }

        $userForm = $this->createForm(
            FinityUserType::class,
            $user
        )->handleRequest($request);

        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin_user_finity_index');
        }

        return $this->render('admin/controllers/user_manager/finity_user_create.html.twig', [
            'userForm' => $userForm->createView(),
        ]);
    }

    /**
     * @param User $user
     * @param Session $session
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}/delete", name="finity_delete")
     */
    public function finityDelete(User $user, Session $session, EntityManagerInterface $entityManager)
    {
        if (!$user->hasRole('ROLE_USER_INFINITY')) {
            $session->getFlashBag()->add('error', 'User has not finity role - ' . $user->getUsername());
            return $this->redirectToRoute('admin_user_finity_index');
        }

        $entityManager->remove($user);
        $entityManager->flush();

        return $this->redirectToRoute('admin_user_finity_index');
    }
}