<?php

namespace App\Controller\Admin;

use App\Entity\Charts\RMBC\RiskMeasuresBenchmarkComparison;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("admin", name="admin_dashboard")
     */
    public function dashboard(EntityManagerInterface $entityManager)
    {
        return $this->render('admin/controllers/dashboard/dashboard.html.twig', [
            'rmbc' => $entityManager->getRepository(RiskMeasuresBenchmarkComparison::class)->findOneBy([])
        ]);
    }
}