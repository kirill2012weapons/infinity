<?php

namespace App\Controller\Api\Chart;

use App\Entity\Charts\AF\AttributionEffect;
use App\Entity\Charts\CTR\ContributionToReturn;
use App\Entity\Charts\PBAC\PerformanceByAssetClass;
use App\Entity\Charts\PBC\CumulativeBenchmarkComparison;
use App\Entity\Charts\PBS\PerformanceBySymbol;
use App\Entity\Charts\SA\SectorAllocation;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @Route("api/chart", name="api_chart_")
 */
class ChartController extends AbstractFOSRestController
{
    /**
     * 1
     *
     * ROUTE - /api/chart/cbc
     *          First chart (BIG)
     *
     * @Rest\Get("/cbc")
     */
    public function cumulativeBenchmarkComparison(EntityManagerInterface $entityManager)
    {
        $data = $entityManager->getRepository(CumulativeBenchmarkComparison::class)->getForApiChart();

        return $this->view($data);
    }

    /**
     * 2
     *
     * ROUTE - /api/chart/af
     *          Second line first chart
     *          Or - Second On full page
     *
     * @Rest\Get("/af")
     */
    public function AttributionEffect(EntityManagerInterface $entityManager)
    {
        $data = $entityManager->getRepository(AttributionEffect::class)->getForApiChart();

        return $this->view($data);
    }

    /**
     * 3
     *
     * ROUTE - /api/chart/sa
     *          Second line second chart
     *          Or - Third On full page
     *
     * @Rest\Get("/sa")
     */
    public function sectorAllocation(EntityManagerInterface $entityManager)
    {
        $data = $entityManager->getRepository(SectorAllocation::class)->getForApiChart();

        return $this->view($data);
    }

    /**
     * 7
     *
     * ROUTE - /api/chart/pbc
     *          7 chart (BIG WITH CIRCLES)
     *
     * @Rest\Get("/pbc")
     */
    public function performanceBySymbol(EntityManagerInterface $entityManager)
    {
        $data = $entityManager->getRepository(PerformanceBySymbol::class)->getForApiChart();

        return $this->view($data);
    }

    /**
     * 4
     *
     * ROUTE - /api/chart/pbac/dates
     *
     * @Rest\Get("/pbac/dates")
     */
    public function performanceByAssetClassDates(EntityManagerInterface $entityManager)
    {
        $data = $entityManager->getRepository(PerformanceByAssetClass::class)->getForApiChartDates();

        return $this->view($data);
    }

    /**
     * 4
     *
     * ROUTE - /api/chart/pbc/contributors/top/{count}
     *          4 chart (TOP)
     *
     * @Rest\Get("/pbc/contributors/top/{count}", defaults={"count":5})
     */
    public function performanceBySymbolContributorsTop($count, EntityManagerInterface $entityManager)
    {
        $data = $entityManager->getRepository(PerformanceBySymbol::class)->getForApiChartContributorsTop($count);

        return $this->view($data);
    }

    /**
     * 4
     *
     * ROUTE - /api/chart/pbc/contributors/bottom/{count}
     *          4 chart (BOTTOM)
     *
     * @Rest\Get("/pbc/contributors/bottom/{count}", defaults={"count":5})
     */
    public function performanceBySymbolContributorsBottom($count, EntityManagerInterface $entityManager)
    {
        $data = $entityManager->getRepository(PerformanceBySymbol::class)->getForApiChartContributorsBottom($count);

        return $this->view($data);
    }

    /**
     * 5
     *
     * ROUTE - /api/chart/ctr/by-sector
     *          5 chart (CONTRIBUTION BY SECTOR)
     *
     * @Rest\Get("/ctr/by-sector")
     */
    public function contributionToReturnBySector(EntityManagerInterface $entityManager)
    {
        $data = $entityManager->getRepository(ContributionToReturn::class)->getForApiChart();

        return $this->view($data);
    }
}