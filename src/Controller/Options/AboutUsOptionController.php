<?php

namespace App\Controller\Options;

use App\Entity\Option\AboutUs\AboutUs;
use App\Entity\Option\AboutUs\AboutUsPart;
use App\Forms\Option\AboutUs\AboutUsType;
use App\Service\Uploader\File;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @Route("admin/options/about", name="admin_options_about_us_")
 */
class AboutUsOptionController extends AbstractController
{
    /**
     * @param EntityManagerInterface $em
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("", name="index")
     */
    public function index(EntityManagerInterface $em)
    {
        return $this->render('admin/controllers/options/about_us/index.html.twig', [
            'result' => $em->getRepository(AboutUs::class)->findLastOne()
        ]);
    }

    /**
     * @param Request $request
     * @param AboutUs $aboutUs
     * @param EntityManagerInterface $entityManager
     * @param File $fileUploader
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(
        Request $request,
        AboutUs $aboutUs,
        EntityManagerInterface $entityManager,
        File $fileUploader
    )
    {

        $aboutUsForm = $this->createForm(
            AboutUsType::class,
            $aboutUs
        )->handleRequest($request);

        if ($aboutUsForm->isSubmitted() && $aboutUsForm->isValid()) {
            foreach ($aboutUs->getAboutUsParts() as $aboutUsPart) {
                if ($aboutUsPart->getImage() instanceof UploadedFile) {
                    $file = $aboutUsPart->getImage();
                    $fileName = $fileUploader->upload($file);

                    $aboutUsPart->setImage($fileName);
                }
            }

            $entityManager->flush();

            return $this->redirectToRoute('admin_options_about_us_index');
        }

        return $this->render('admin/controllers/options/about_us/edit.html.twig', [
            'form' => $aboutUsForm->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param File $fileUploader
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/create", name="create")
     */
    public function create(
        Request $request,
        EntityManagerInterface $entityManager,
        File $fileUploader
    )
    {
        $aboutUs = (new AboutUs())
            ->addAboutUsPart((new AboutUsPart()))
        ;

        $aboutUsForm = $this->createForm(
            AboutUsType::class,
            $aboutUs
        )->handleRequest($request);

        if ($aboutUsForm->isSubmitted() && $aboutUsForm->isValid()) {
            foreach ($aboutUs->getAboutUsParts() as $aboutUsPart) {
                if ($aboutUsPart->getImage() instanceof UploadedFile) {
                    $file = $aboutUsPart->getImage();
                    $fileName = $fileUploader->upload($file);

                    $aboutUsPart->setImage($fileName);
                }
            }

            $entityManager->persist($aboutUs);
            $entityManager->flush();

            return $this->redirectToRoute('admin_options_about_us_index');
        }

        return $this->render('admin/controllers/options/about_us/create.html.twig', [
            'form' => $aboutUsForm->createView()
        ]);
    }

    /**
     * @param AboutUs $aboutUs
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}/delete", name="delete")
     */
    public function delete(
        AboutUs $aboutUs,
        EntityManagerInterface $entityManager
    )
    {
        $entityManager->remove($aboutUs);
        $entityManager->flush();

        return $this->redirectToRoute('admin_options_about_us_index');
    }
}