<?php

namespace App\Controller\Options;

use App\Entity\Option\Performance\PerformanceContentOption;
use App\Entity\Option\Performance\PerformanceOption;
use App\Forms\Option\Performance\PerformanceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @Route("admin/options/performance", name="admin_options_performance_")
 */
class PerformanceOptionController extends AbstractController
{
    /**
     * @param EntityManagerInterface $em
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("", name="index")
     */
    public function index(EntityManagerInterface $em)
    {
        return $this->render('admin/controllers/options/performance/index.html.twig', [
            'result' => $em->getRepository(PerformanceOption::class)->getPerformance()
        ]);
    }

    /**
     * @param Request $request
     * @param PerformanceOption $performanceOption
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(
        Request $request,
        PerformanceOption $performanceOption,
        EntityManagerInterface $entityManager
    ) {
        $performanceContentForm = $this->createForm(
            PerformanceType::class,
            $performanceOption
        )->handleRequest($request);

        if ($performanceContentForm->isSubmitted() && $performanceContentForm->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin_options_performance_index');
        }

        return $this->render('admin/controllers/options/performance/edit.html.twig', [
            'form' => $performanceContentForm->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/create", name="create")
     */
    public function create(
        Request $request,
        EntityManagerInterface $entityManager
    ) {
        $performanceOption = (new PerformanceOption())
            ->addPerformanceContent((new PerformanceContentOption()))
        ;

        $performanceContentForm = $this->createForm(
            PerformanceType::class,
            $performanceOption
        )->handleRequest($request);

        if ($performanceContentForm->isSubmitted() && $performanceContentForm->isValid()) {
            $entityManager->persist($performanceOption);
            $entityManager->flush();

            return $this->redirectToRoute('admin_options_performance_index');
        }

        return $this->render('admin/controllers/options/performance/create.html.twig', [
            'form' => $performanceContentForm->createView()
        ]);
    }

    /**
     * @param PerformanceOption $performanceOption
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}/delete", name="delete")
     */
    public function delete(
        PerformanceOption $performanceOption,
        EntityManagerInterface $entityManager
    )
    {
        $entityManager->remove($performanceOption);
        $entityManager->flush();

        return $this->redirectToRoute('admin_options_performance_index');
    }
}