<?php

namespace App\Controller\Chart\PBAC;

use App\Entity\Charts\CTR\Sector;
use App\Entity\Charts\PBAC\PerformanceByAssetClass;
use App\Entity\Charts\PBC\Consolidated;
use App\Entity\Charts\PBC\CumulativeBenchmarkComparison;
use App\Entity\Charts\PBC\Efa;
use App\Entity\Charts\PBC\Spxtr;
use App\Entity\Charts\PBC\Vt;
use App\Entity\Charts\PBS\PerformanceBySymbol;
use App\Entity\Option\Performance\PerformanceOption;
use App\Forms\Chart\CTR\SectorType;
use App\Forms\Chart\PBAC\PerformanceByAssetClassType;
use App\Forms\Option\Performance\Charts\TitlePBACType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/pbac", name="admin_pbac_")
 */
class PBACController extends AbstractController
{
    /**
     * @param EntityManagerInterface $em
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("", name="index")
     */
    public function index(EntityManagerInterface $em, Request $request)
    {
        $performanceOption = $em->getRepository(PerformanceOption::class)->getPerformance();
        $exist             = true;

        if (null === $performanceOption) {
            $exist = false;
            $performanceOption = new PerformanceOption();
        }

        $titleForm   = $this->createForm(
            TitlePBACType::class,
            $performanceOption
        )->handleRequest($request);

        if ($titleForm->isSubmitted() && $titleForm->isValid()) {
            if (!$exist) {
                $em->persist($performanceOption);
            }

            $em->flush();

            return $this->redirectToRoute('admin_pbac_index');
        }

        return $this->render('admin/controllers/chart/pbac/index.html.twig', [
            'pbacResult' => $em->getRepository(PerformanceByAssetClass::class)->findAll(),
            'titleForm'  => $titleForm->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param PerformanceByAssetClass $performanceByAssetClass
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(
        Request $request,
        PerformanceByAssetClass $performanceByAssetClass,
        EntityManagerInterface $entityManager
    )
    {
        $performanceByAssetClassForm = $this->createForm(
            PerformanceByAssetClassType::class,
            $performanceByAssetClass
        )->handleRequest($request);

        if ($performanceByAssetClassForm->isSubmitted() && $performanceByAssetClassForm->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin_pbac_index');
        }

        return $this->render('admin/controllers/chart/pbac/edit.html.twig', [
            'performanceByAssetClassForm' => $performanceByAssetClassForm->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/create", name="create")
     */
    public function create(
        Request $request,
        EntityManagerInterface $entityManager
    )
    {
        $performanceByAssetClass = new PerformanceByAssetClass();

        $performanceByAssetClassForm = $this->createForm(
            PerformanceByAssetClassType::class,
            $performanceByAssetClass
        )->handleRequest($request);

        if ($performanceByAssetClassForm->isSubmitted() && $performanceByAssetClassForm->isValid()) {
            $entityManager->persist($performanceByAssetClass);
            $entityManager->flush();

            return $this->redirectToRoute('admin_pbac_index');
        }

        return $this->render('admin/controllers/chart/pbac/create.html.twig', [
            'performanceByAssetClassForm' => $performanceByAssetClassForm->createView()
        ]);
    }

    /**
     * @param PerformanceByAssetClass $performanceByAssetClass
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}/delete", name="delete")
     */
    public function delete(
        PerformanceByAssetClass $performanceByAssetClass,
        EntityManagerInterface $entityManager
    )
    {
        $entityManager->remove($performanceByAssetClass);
        $entityManager->flush();

        return $this->redirectToRoute('admin_pbac_index');
    }
}