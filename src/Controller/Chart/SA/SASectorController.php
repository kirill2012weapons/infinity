<?php

namespace App\Controller\Chart\SA;

use App\Entity\Charts\SA\Sector;
use App\Forms\Chart\SA\SectorType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/sa/sector", name="admin_sa_sector_")
 */
class SASectorController extends AbstractController
{
    /**
     * @param EntityManagerInterface $em
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("", name="index")
     */
    public function index(EntityManagerInterface $em)
    {
        return $this->render('admin/controllers/chart/sa/sector/index.html.twig', [
            'seSectorResult' => $em->getRepository(Sector::class)->findAll()
        ]);
    }

    /**
     * @param Request $request
     * @param Sector $sector
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(
        Request $request,
        Sector $sector,
        EntityManagerInterface $entityManager
    )
    {
        $sectorForm = $this->createForm(
            SectorType::class,
            $sector
        )->handleRequest($request);

        if ($sectorForm->isSubmitted() && $sectorForm->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin_sa_sector_index');
        }

        return $this->render('admin/controllers/chart/sa/sector/edit.html.twig', [
            'sectorForm' => $sectorForm->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/create", name="create")
     */
    public function create(
        Request $request,
        EntityManagerInterface $entityManager
    )
    {
        $sector = new Sector();

        $sectorForm = $this->createForm(
            SectorType::class,
            $sector
        )->handleRequest($request);

        if ($sectorForm->isSubmitted() && $sectorForm->isValid()) {
            $entityManager->persist($sector);
            $entityManager->flush();

            return $this->redirectToRoute('admin_sa_sector_index');
        }

        return $this->render('admin/controllers/chart/sa/sector/create.html.twig', [
            'sectorForm' => $sectorForm->createView()
        ]);
    }

    /**
     * @param Sector $sector
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}/delete", name="delete")
     */
    public function delete(
        Sector $sector,
        EntityManagerInterface $entityManager
    )
    {
        $entityManager->remove($sector);
        $entityManager->flush();

        return $this->redirectToRoute('admin_sa_sector_index');
    }
}