<?php

namespace App\Controller\Chart\SA;

use App\Entity\Charts\SA\SectorAllocation;
use App\Entity\Option\Performance\PerformanceOption;
use App\Forms\Chart\SA\SectorAllocationType;
use App\Forms\Option\Performance\Charts\TitleSAType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/sa", name="admin_sa_")
 */
class SAController extends AbstractController
{
    /**
     * @param EntityManagerInterface $em
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("", name="index")
     */
    public function index(EntityManagerInterface $em, Request $request)
    {
        $performanceOption = $em->getRepository(PerformanceOption::class)->getPerformance();
        $exist             = true;

        if (null === $performanceOption) {
            $exist = false;
            $performanceOption = new PerformanceOption();
        }

        $titleForm   = $this->createForm(
            TitleSAType::class,
            $performanceOption
        )->handleRequest($request);

        if ($titleForm->isSubmitted() && $titleForm->isValid()) {
            if (!$exist) {
                $em->persist($performanceOption);
            }

            $em->flush();

            return $this->redirectToRoute('admin_sa_index');
        }

        return $this->render('admin/controllers/chart/sa/index.html.twig', [
            'saResult'  => $em->getRepository(SectorAllocation::class)->findAll(),
            'titleForm' => $titleForm->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param SectorAllocation $sectorAllocation
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(
        Request $request,
        SectorAllocation $sectorAllocation,
        EntityManagerInterface $entityManager
    )
    {
        $SectorAllocationForm = $this->createForm(
            SectorAllocationType::class,
            $sectorAllocation
        )->handleRequest($request);

        if ($SectorAllocationForm->isSubmitted() && $SectorAllocationForm->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin_sa_index');
        }

        return $this->render('admin/controllers/chart/sa/edit.html.twig', [
            'sectorAllocationForm' => $SectorAllocationForm->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/create", name="create")
     */
    public function create(
        Request $request,
        EntityManagerInterface $entityManager
    )
    {
        $sectorAllocation = (new SectorAllocation());

        $sectorAllocationForm = $this->createForm(
            SectorAllocationType::class,
            $sectorAllocation
        )->handleRequest($request);

        if ($sectorAllocationForm->isSubmitted() && $sectorAllocationForm->isValid()) {
            $entityManager->persist($sectorAllocation);
            $entityManager->flush();

            return $this->redirectToRoute('admin_sa_index');
        }

        return $this->render('admin/controllers/chart/sa/create.html.twig', [
            'sectorAllocationForm' => $sectorAllocationForm->createView()
        ]);
    }

    /**
     * @param SectorAllocation $SectorAllocation
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}/delete", name="delete")
     */
    public function delete(
        SectorAllocation $sectorAllocation,
        EntityManagerInterface $entityManager
    )
    {
        $entityManager->remove($sectorAllocation);
        $entityManager->flush();

        return $this->redirectToRoute('admin_sa_index');
    }
}