<?php

namespace App\Controller\Chart\RMBC;

use App\Entity\Charts\RMBC\Consolidated;
use App\Entity\Charts\RMBC\Efa;
use App\Entity\Charts\RMBC\RiskMeasuresBenchmarkComparison;
use App\Entity\Charts\RMBC\Spxtr;
use App\Entity\Charts\RMBC\Vt;
use App\Entity\Option\Performance\PerformanceOption;
use App\Forms\Chart\RMBC\RiskMeasuresBenchmarkComparisonType;
use App\Forms\Chart\RMBC\TypeNamesType;
use App\Forms\Option\Performance\Charts\TitleRMBCType;
use App\Manager\Chart\RMBC\NameManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/rmbc", name="admin_rmbc_")
 */
class RMBCController extends AbstractController
{
    /**
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param NameManager $nameManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @Route("", name="index")
     */
    public function index(
        EntityManagerInterface $em,
        Request $request,
        NameManager $nameManager
    ) {
        $typeNames    = $nameManager->getNames();
        $typeNameForm = $this->createForm(TypeNamesType::class, $typeNames)
            ->handleRequest($request);

        if ($typeNameForm->isSubmitted() && $typeNameForm->isValid()) {
            $nameManager->updateName($typeNames);

            return $this->redirectToRoute('admin_rmbc_index');
        }

        $performanceOption = $em->getRepository(PerformanceOption::class)->getPerformance();
        $exist             = true;

        if (null === $performanceOption) {
            $exist = false;
            $performanceOption = new PerformanceOption();
        }

        $titleForm   = $this->createForm(
            TitleRMBCType::class,
            $performanceOption
        )->handleRequest($request);

        if ($titleForm->isSubmitted() && $titleForm->isValid()) {
            if (!$exist) {
                $em->persist($performanceOption);
            }

            $em->flush();

            return $this->redirectToRoute('admin_rmbc_index');
        }

        return $this->render('admin/controllers/chart/rmbc/index.html.twig', [
            'rmbcResult'        => $em->getRepository(RiskMeasuresBenchmarkComparison::class)->findAll(),
            'rmbcResultsCharts' => $em->getRepository(RiskMeasuresBenchmarkComparison::class)->findOneBy([]),
            'titleForm'         => $titleForm->createView(),
            'typeNameForm' => $typeNameForm->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param RiskMeasuresBenchmarkComparison $riskMeasuresBenchmarkComparison
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(
        Request $request,
        RiskMeasuresBenchmarkComparison $riskMeasuresBenchmarkComparison,
        EntityManagerInterface $entityManager,
        NameManager $nameManager
    )
    {
        $riskMeasuresBenchmarkComparisonForm = $this->createForm(
            RiskMeasuresBenchmarkComparisonType::class,
            $riskMeasuresBenchmarkComparison
        )->handleRequest($request);

        if ($riskMeasuresBenchmarkComparisonForm->isSubmitted() && $riskMeasuresBenchmarkComparisonForm->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin_rmbc_index');
        }

        return $this->render('admin/controllers/chart/rmbc/edit.html.twig', [
            'riskMeasuresBenchmarkComparisonForm' => $riskMeasuresBenchmarkComparisonForm->createView(),
            'riskMeasuresBenchmarkComparison'     => $riskMeasuresBenchmarkComparison,
            'names'                               => $nameManager->getNames()
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/create", name="create")
     */
    public function create(
        Request $request,
        EntityManagerInterface $entityManager
    )
    {
        $riskMeasuresBenchmarkComparison = (new RiskMeasuresBenchmarkComparison())
            ->setSpxtr(new Spxtr())
            ->setEfa(new Efa())
            ->setVt(new Vt())
            ->setConsolidated(new Consolidated());

        $riskMeasuresBenchmarkComparisonForm = $this->createForm(
            RiskMeasuresBenchmarkComparisonType::class,
            $riskMeasuresBenchmarkComparison
        )->handleRequest($request);

        if ($riskMeasuresBenchmarkComparisonForm->isSubmitted() && $riskMeasuresBenchmarkComparisonForm->isValid()) {
            $entityManager->persist($riskMeasuresBenchmarkComparison);
            $entityManager->flush();

            return $this->redirectToRoute('admin_rmbc_index');
        }

        return $this->render('admin/controllers/chart/rmbc/create.html.twig', [
            'riskMeasuresBenchmarkComparisonForm' => $riskMeasuresBenchmarkComparisonForm->createView()
        ]);
    }

    /**
     * @param RiskMeasuresBenchmarkComparison $riskMeasuresBenchmarkComparison
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}/delete", name="delete")
     */
    public function delete(
        RiskMeasuresBenchmarkComparison $riskMeasuresBenchmarkComparison,
        EntityManagerInterface $entityManager
    )
    {
        $entityManager->remove($riskMeasuresBenchmarkComparison);
        $entityManager->flush();

        return $this->redirectToRoute('admin_rmbc_index');
    }
}