<?php

namespace App\Controller\Chart\CTR;

use App\Entity\Charts\CTR\Sector;
use App\Entity\Charts\PBC\Consolidated;
use App\Entity\Charts\PBC\CumulativeBenchmarkComparison;
use App\Entity\Charts\PBC\Efa;
use App\Entity\Charts\PBC\Spxtr;
use App\Entity\Charts\PBC\Vt;
use App\Forms\Chart\CTR\SectorType;
use App\Forms\Chart\PBAC\PerformanceByAssetClassType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/ctr/sector", name="admin_ctr_sector_")
 */
class CTRSectorController extends AbstractController
{
    /**
     * @param EntityManagerInterface $em
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("", name="index")
     */
    public function index(EntityManagerInterface $em)
    {
        return $this->render('admin/controllers/chart/ctr/sector/index.html.twig', [
            'ctrSectorResult' => $em->getRepository(Sector::class)->findAll()
        ]);
    }

    /**
     * @param Request $request
     * @param Sector $sector
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(
        Request $request,
        Sector $sector,
        EntityManagerInterface $entityManager
    )
    {
        $sectorForm = $this->createForm(
            SectorType::class,
            $sector
        )->handleRequest($request);

        if ($sectorForm->isSubmitted() && $sectorForm->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin_ctr_sector_index');
        }

        return $this->render('admin/controllers/chart/ctr/sector/edit.html.twig', [
            'sectorForm' => $sectorForm->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/create", name="create")
     */
    public function create(
        Request $request,
        EntityManagerInterface $entityManager
    )
    {
        $sector = new Sector();

        $sectorForm = $this->createForm(
            SectorType::class,
            $sector
        )->handleRequest($request);

        if ($sectorForm->isSubmitted() && $sectorForm->isValid()) {
            $entityManager->persist($sector);
            $entityManager->flush();

            return $this->redirectToRoute('admin_ctr_sector_index');
        }

        return $this->render('admin/controllers/chart/ctr/sector/create.html.twig', [
            'sectorForm' => $sectorForm->createView()
        ]);
    }

    /**
     * @param Sector $sector
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}/delete", name="delete")
     */
    public function delete(
        Sector $sector,
        EntityManagerInterface $entityManager
    )
    {
        $entityManager->remove($sector);
        $entityManager->flush();

        return $this->redirectToRoute('admin_ctr_sector_index');
    }
}