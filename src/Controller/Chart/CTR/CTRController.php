<?php

namespace App\Controller\Chart\CTR;

use App\Entity\Charts\CTR\ContributionToReturn;
use App\Entity\Charts\CTR\Sector;
use App\Entity\Charts\PBC\Consolidated;
use App\Entity\Charts\PBC\CumulativeBenchmarkComparison;
use App\Entity\Charts\PBC\Efa;
use App\Entity\Charts\PBC\Spxtr;
use App\Entity\Charts\PBC\Vt;
use App\Entity\Option\Performance\PerformanceOption;
use App\Forms\Chart\CTR\ContributionToReturnType;
use App\Forms\Chart\PBAC\PerformanceByAssetClassType;
use App\Forms\Option\Performance\Charts\TitleCTRType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/ctr", name="admin_ctr_")
 */
class CTRController extends AbstractController
{
    /**
     * @param EntityManagerInterface $em
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("", name="index")
     */
    public function index(EntityManagerInterface $em, Request $request)
    {
        $performanceOption = $em->getRepository(PerformanceOption::class)->getPerformance();
        $exist             = true;

        if (null === $performanceOption) {
            $exist = false;
            $performanceOption = new PerformanceOption();
        }

        $titleForm   = $this->createForm(
            TitleCTRType::class,
            $performanceOption
        )->handleRequest($request);

        if ($titleForm->isSubmitted() && $titleForm->isValid()) {
            if (!$exist) {
                $em->persist($performanceOption);
            }

            $em->flush();

            return $this->redirectToRoute('admin_ctr_index');
        }

        return $this->render('admin/controllers/chart/ctr/index.html.twig', [
            'ctrResult' => $em->getRepository(ContributionToReturn::class)->findAll(),
            'titleForm' => $titleForm->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param ContributionToReturn $contributionToReturn
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(
        Request $request,
        ContributionToReturn $contributionToReturn,
        EntityManagerInterface $entityManager
    )
    {
        $contributionToReturnForm = $this->createForm(
            ContributionToReturnType::class,
            $contributionToReturn
        )->handleRequest($request);

        if ($contributionToReturnForm->isSubmitted() && $contributionToReturnForm->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin_ctr_index');
        }

        return $this->render('admin/controllers/chart/ctr/edit.html.twig', [
            'contributionToReturnForm' => $contributionToReturnForm->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/create", name="create")
     */
    public function create(
        Request $request,
        EntityManagerInterface $entityManager
    )
    {
        $contributionToReturn = (new ContributionToReturn());

        $contributionToReturnForm = $this->createForm(
            ContributionToReturnType::class,
            $contributionToReturn
        )->handleRequest($request);


        if ($contributionToReturnForm->isSubmitted() && $contributionToReturnForm->isValid()) {
            $entityManager->persist($contributionToReturn);
            $entityManager->flush();

            return $this->redirectToRoute('admin_ctr_index');
        }

        return $this->render('admin/controllers/chart/ctr/create.html.twig', [
            'contributionToReturnForm' => $contributionToReturnForm->createView()
        ]);
    }

    /**
     * @param ContributionToReturn $contributionToReturn
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}/delete", name="delete")
     */
    public function delete(
        ContributionToReturn $contributionToReturn,
        EntityManagerInterface $entityManager
    )
    {
        $entityManager->remove($contributionToReturn);
        $entityManager->flush();

        return $this->redirectToRoute('admin_ctr_index');
    }
}