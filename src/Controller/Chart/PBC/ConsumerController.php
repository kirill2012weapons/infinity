<?php

namespace App\Controller\Chart\PBC;

use App\Entity\Charts\PBS\ConsumerCyc;
use App\Forms\Chart\PBS\ConsumerCycType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/pbs/consumer", name="admin_pbs_consumer_")
 */
class ConsumerController extends AbstractController
{
    /**
     * @param EntityManagerInterface $em
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("", name="index")
     */
    public function index(EntityManagerInterface $em)
    {
        return $this->render('admin/controllers/chart/pbs/consumer/index.html.twig', [
            'consumerResult' => $em->getRepository(ConsumerCyc::class)->findAll()
        ]);
    }

    /**
     * @param Request $request
     * @param ConsumerCyc $consumerCyc
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(
        Request $request,
        ConsumerCyc $consumerCyc,
        EntityManagerInterface $entityManager
    )
    {
        $consumerCycForm = $this->createForm(
            ConsumerCycType::class,
            $consumerCyc
        )->handleRequest($request);

        if ($consumerCycForm->isSubmitted() && $consumerCycForm->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin_pbs_consumer_index');
        }

        return $this->render('admin/controllers/chart/pbs/consumer/edit.html.twig', [
            'consumerCycForm' => $consumerCycForm->createView(),
            'consumerCyc'     => $consumerCyc
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/create", name="create")
     */
    public function create(
        Request $request,
        EntityManagerInterface $entityManager
    )
    {
        $consumer = new ConsumerCyc();

        $consumerCycForm = $this->createForm(
            ConsumerCycType::class,
            $consumer
        )->handleRequest($request);

        if ($consumerCycForm->isSubmitted() && $consumerCycForm->isValid()) {
            $entityManager->persist($consumer);
            $entityManager->flush();

            return $this->redirectToRoute('admin_pbs_consumer_index');
        }

        return $this->render('admin/controllers/chart/pbs/consumer/create.html.twig', [
            'consumerCycForm' => $consumerCycForm->createView()
        ]);
    }

    /**
     * @param ConsumerCyc $consumerCyc
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}/delete", name="delete")
     */
    public function delete(
        ConsumerCyc $consumerCyc,
        EntityManagerInterface $entityManager
    )
    {
        $entityManager->remove($consumerCyc);
        $entityManager->flush();

        return $this->redirectToRoute('admin_pbs_consumer_index');
    }
}