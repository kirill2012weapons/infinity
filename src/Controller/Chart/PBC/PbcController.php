<?php

namespace App\Controller\Chart\PBC;

use App\Entity\Charts\PBC\Consolidated;
use App\Entity\Charts\PBC\CumulativeBenchmarkComparison;
use App\Entity\Charts\PBC\Efa;
use App\Entity\Charts\PBC\Spxtr;
use App\Entity\Charts\PBC\Vt;
use App\Entity\Option\Performance\PerformanceOption;
use App\Forms\Chart\PBC\CumulativeBenchmarkComparisonType;
use App\Forms\Chart\PBC\TypeNamesType;
use App\Forms\Option\Performance\Charts\TitlePBCType;
use App\Manager\Chart\PBC\NameManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/pbc", name="admin_pbc_")
 */
class PbcController extends AbstractController
{
    /**
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param NameManager $nameManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @Route("", name="index")
     */
    public function index(
        EntityManagerInterface $em,
        Request $request,
        NameManager $nameManager
    ) {
        $typeNames    = $nameManager->getNames();
        $typeNameForm = $this->createForm(TypeNamesType::class, $typeNames)
            ->handleRequest($request);

        if ($typeNameForm->isSubmitted() && $typeNameForm->isValid()) {
            $nameManager->updateName($typeNames);

            return $this->redirectToRoute('admin_pbc_index');
        }

        $performanceOption = $em->getRepository(PerformanceOption::class)->getPerformance();
        $exist             = true;

        if (null === $performanceOption) {
            $exist = false;
            $performanceOption = new PerformanceOption();
        }

        $titleForm   = $this->createForm(
            TitlePBCType::class,
            $performanceOption
        )->handleRequest($request);

        if ($titleForm->isSubmitted() && $titleForm->isValid()) {
            if (!$exist) {
                $em->persist($performanceOption);
            }

            $em->flush();

            return $this->redirectToRoute('admin_pbc_index');
        }

        return $this->render('admin/controllers/chart/pbc/index.html.twig', [
            'pbcResult'    => $em->getRepository(CumulativeBenchmarkComparison::class)->findAll(),
            'titleForm'    => $titleForm->createView(),
            'typeNameForm' => $typeNameForm->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param CumulativeBenchmarkComparison $comparison
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(
        Request $request,
        CumulativeBenchmarkComparison $comparison,
        EntityManagerInterface $entityManager
    )
    {
        $comparisonForm = $this->createForm(
            CumulativeBenchmarkComparisonType::class,
            $comparison
        )->handleRequest($request);

        if ($comparisonForm->isSubmitted() && $comparisonForm->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin_pbc_index');
        }

        return $this->render('admin/controllers/chart/pbc/edit.html.twig', [
            'comparisonForm' => $comparisonForm->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/create", name="create")
     */
    public function create(
        Request $request,
        EntityManagerInterface $entityManager
    )
    {
        $comparison = (new CumulativeBenchmarkComparison())
            ->setSpxtr(new Spxtr())
            ->setEfa(new Efa())
            ->setVt(new Vt())
            ->setConsolidated(new Consolidated());

        $comparisonForm = $this->createForm(
            CumulativeBenchmarkComparisonType::class,
            $comparison
        )->handleRequest($request);

        if ($comparisonForm->isSubmitted() && $comparisonForm->isValid()) {
            $entityManager->persist($comparison);
            $entityManager->flush();

            return $this->redirectToRoute('admin_pbc_index');
        }

        return $this->render('admin/controllers/chart/pbc/create.html.twig', [
            'comparisonForm' => $comparisonForm->createView()
        ]);
    }

    /**
     * @param CumulativeBenchmarkComparison $comparison
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}/delete", name="delete")
     */
    public function delete(
        CumulativeBenchmarkComparison $comparison,
        EntityManagerInterface $entityManager
    )
    {
        $entityManager->remove($comparison);
        $entityManager->flush();

        return $this->redirectToRoute('admin_pbc_index');
    }
}