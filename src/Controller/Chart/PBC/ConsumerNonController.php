<?php

namespace App\Controller\Chart\PBC;

use App\Entity\Charts\PBS\ConsumerNonCyc;
use App\Forms\Chart\PBS\ConsumerNonCycType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/pbs/consumer-non", name="admin_pbs_consumer_non_")
 */
class ConsumerNonController extends AbstractController
{
    /**
     * @param EntityManagerInterface $em
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("", name="index")
     */
    public function index(EntityManagerInterface $em)
    {
        return $this->render('admin/controllers/chart/pbs/consumer_non/index.html.twig', [
            'consumerNonResult' => $em->getRepository(ConsumerNonCyc::class)->findAll()
        ]);
    }

    /**
     * @param Request $request
     * @param ConsumerNonCyc $consumerNonCyc
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(
        Request $request,
        ConsumerNonCyc $consumerNonCyc,
        EntityManagerInterface $entityManager
    )
    {
        $consumerNonCycForm = $this->createForm(
            ConsumerNonCycType::class,
            $consumerNonCyc
        )->handleRequest($request);

        if ($consumerNonCycForm->isSubmitted() && $consumerNonCycForm->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin_pbs_consumer_non_index');
        }

        return $this->render('admin/controllers/chart/pbs/consumer_non/edit.html.twig', [
            'consumerNonCycForm' => $consumerNonCycForm->createView(),
            'consumerNonCyc'     => $consumerNonCycForm
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/create", name="create")
     */
    public function create(
        Request $request,
        EntityManagerInterface $entityManager
    )
    {
        $consumerNon = new ConsumerNonCyc();

        $consumerNonCycForm = $this->createForm(
            ConsumerNonCycType::class,
            $consumerNon
        )->handleRequest($request);

        if ($consumerNonCycForm->isSubmitted() && $consumerNonCycForm->isValid()) {
            $entityManager->persist($consumerNon);
            $entityManager->flush();

            return $this->redirectToRoute('admin_pbs_consumer_non_index');
        }

        return $this->render('admin/controllers/chart/pbs/consumer_non/create.html.twig', [
            'consumerNonCycForm' => $consumerNonCycForm->createView()
        ]);
    }

    /**
     * @param ConsumerNonCyc $consumerNonCyc
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}/delete", name="delete")
     */
    public function delete(
        ConsumerNonCyc $consumerNonCyc,
        EntityManagerInterface $entityManager
    )
    {
        $entityManager->remove($consumerNonCyc);
        $entityManager->flush();

        return $this->redirectToRoute('admin_pbs_consumer_non_index');
    }
}