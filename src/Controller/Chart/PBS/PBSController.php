<?php

namespace App\Controller\Chart\PBS;

use App\Entity\Charts\PBS\PerformanceBySymbol;
use App\Entity\Option\Performance\PerformanceOption;
use App\Forms\Chart\PBS\PerformanceBySymbolType;
use App\Forms\Option\Performance\Charts\TitlePBSType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/pbs", name="admin_pbs_")
 */
class PBSController extends AbstractController
{
    /**
     * @param EntityManagerInterface $em
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @Route("", name="index")
     */
    public function index(
        EntityManagerInterface $em,
        Request $request
    ) {
        $performanceOption = $em->getRepository(PerformanceOption::class)->getPerformance();
        $exist             = true;

        if (null === $performanceOption) {
            $exist = false;
            $performanceOption = new PerformanceOption();
        }

        $titleForm   = $this->createForm(
            TitlePBSType::class,
            $performanceOption
        )->handleRequest($request);

        if ($titleForm->isSubmitted() && $titleForm->isValid()) {
            if (!$exist) {
                $em->persist($performanceOption);
            }

            $em->flush();

            return $this->redirectToRoute('admin_pbs_index');
        }

        return $this->render('admin/controllers/chart/pbs/index.html.twig', [
            'pbsResult' => $em->getRepository(PerformanceBySymbol::class)->findAll(),
            'titleForm' => $titleForm->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param PerformanceBySymbol $performanceBySymbol
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(
        Request $request,
        PerformanceBySymbol $performanceBySymbol,
        EntityManagerInterface $entityManager
    ) {
        $performanceBySymbolForm = $this->createForm(
            PerformanceBySymbolType::class,
            $performanceBySymbol
        )->handleRequest($request);

        if ($performanceBySymbolForm->isSubmitted() && $performanceBySymbolForm->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin_pbs_index');
        }

        return $this->render('admin/controllers/chart/pbs/edit.html.twig', [
            'performanceBySymbolForm' => $performanceBySymbolForm->createView(),
            'performanceBySymbol'     => $performanceBySymbol
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/create", name="create")
     */
    public function create(
        Request $request,
        EntityManagerInterface $entityManager
    )
    {
        $pbs = new PerformanceBySymbol();

        $pbsForm = $this->createForm(
            PerformanceBySymbolType::class,
            $pbs
        )->handleRequest($request);

        if ($pbsForm->isSubmitted() && $pbsForm->isValid()) {
            $entityManager->persist($pbs);
            $entityManager->flush();

            return $this->redirectToRoute('admin_pbs_index');
        }

        return $this->render('admin/controllers/chart/pbs/create.html.twig', [
            'pbsForm' => $pbsForm->createView()
        ]);
    }

    /**
     * @param PerformanceBySymbol $performanceBySymbol
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}/delete", name="delete")
     */
    public function delete(
        PerformanceBySymbol $performanceBySymbol,
        EntityManagerInterface $entityManager
    )
    {
        $entityManager->remove($performanceBySymbol);
        $entityManager->flush();

        return $this->redirectToRoute('admin_pbs_index');
    }
}