<?php

namespace App\Controller\Chart\AF;

use App\Entity\Charts\AF\Sector;
use App\Forms\Chart\AF\SectorType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/af/sector", name="admin_af_sector_")
 */
class AFSectorController extends AbstractController
{
    /**
     * @param EntityManagerInterface $em
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("", name="index")
     */
    public function index(EntityManagerInterface $em)
    {
        return $this->render('admin/controllers/chart/af/sector/index.html.twig', [
            'afSectorResult' => $em->getRepository(Sector::class)->findAll()
        ]);
    }

    /**
     * @param Request $request
     * @param Sector $sector
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(
        Request $request,
        Sector $sector,
        EntityManagerInterface $entityManager
    )
    {
        $sectorForm = $this->createForm(
            SectorType::class,
            $sector
        )->handleRequest($request);

        if ($sectorForm->isSubmitted() && $sectorForm->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin_af_sector_index');
        }

        return $this->render('admin/controllers/chart/af/sector/edit.html.twig', [
            'sectorForm' => $sectorForm->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/create", name="create")
     */
    public function create(
        Request $request,
        EntityManagerInterface $entityManager
    )
    {
        $sector = new Sector();

        $sectorForm = $this->createForm(
            SectorType::class,
            $sector
        )->handleRequest($request);

        if ($sectorForm->isSubmitted() && $sectorForm->isValid()) {
            $entityManager->persist($sector);
            $entityManager->flush();

            return $this->redirectToRoute('admin_af_sector_index');
        }

        return $this->render('admin/controllers/chart/af/sector/create.html.twig', [
            'sectorForm' => $sectorForm->createView()
        ]);
    }

    /**
     * @param Sector $sector
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}/delete", name="delete")
     */
    public function delete(
        Sector $sector,
        EntityManagerInterface $entityManager
    )
    {
        $entityManager->remove($sector);
        $entityManager->flush();

        return $this->redirectToRoute('admin_af_sector_index');
    }
}