<?php

namespace App\Controller\Chart\AF;

use App\Entity\Charts\AF\AttributionEffect;
use App\Entity\Option\Performance\PerformanceOption;
use App\Forms\Chart\AF\AttributionEffectType;
use App\Forms\Option\Performance\Charts\TitleAFType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/af", name="admin_af_")
 */
class AFController extends AbstractController
{
    /**
     * @param EntityManagerInterface $em
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("", name="index")
     */
    public function index(EntityManagerInterface $em, Request $request)
    {
        $performanceOption = $em->getRepository(PerformanceOption::class)->getPerformance();
        $exist             = true;

        if (null === $performanceOption) {
            $exist = false;
            $performanceOption = new PerformanceOption();
        }

        $titleForm   = $this->createForm(
            TitleAFType::class,
            $performanceOption
        )->handleRequest($request);

        if ($titleForm->isSubmitted() && $titleForm->isValid()) {
            if (!$exist) {
                $em->persist($performanceOption);
            }

            $em->flush();

            return $this->redirectToRoute('admin_af_index');
        }

        return $this->render('admin/controllers/chart/af/index.html.twig', [
            'afResult' => $em->getRepository(AttributionEffect::class)->findAll(),
            'titleForm' => $titleForm->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param AttributionEffect $attributionEffect
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(
        Request $request,
        AttributionEffect $attributionEffect,
        EntityManagerInterface $entityManager
    )
    {
        $attributionEffectForm = $this->createForm(
            AttributionEffectType::class,
            $attributionEffect
        )->handleRequest($request);

        if ($attributionEffectForm->isSubmitted() && $attributionEffectForm->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin_af_index');
        }

        return $this->render('admin/controllers/chart/af/edit.html.twig', [
            'attributionEffectForm' => $attributionEffectForm->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/create", name="create")
     */
    public function create(
        Request $request,
        EntityManagerInterface $entityManager
    )
    {
        $attributionEffect = (new AttributionEffect());

        $attributionEffectForm = $this->createForm(
            AttributionEffectType::class,
            $attributionEffect
        )->handleRequest($request);

        if ($attributionEffectForm->isSubmitted() && $attributionEffectForm->isValid()) {
            $entityManager->persist($attributionEffect);
            $entityManager->flush();

            return $this->redirectToRoute('admin_af_index');
        }

        return $this->render('admin/controllers/chart/af/create.html.twig', [
            'attributionEffectForm' => $attributionEffectForm->createView()
        ]);
    }

    /**
     * @param AttributionEffect $attributionEffect
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}/delete", name="delete")
     */
    public function delete(
        AttributionEffect $attributionEffect,
        EntityManagerInterface $entityManager
    )
    {
        $entityManager->remove($attributionEffect);
        $entityManager->flush();

        return $this->redirectToRoute('admin_af_index');
    }
}