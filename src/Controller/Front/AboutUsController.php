<?php

namespace App\Controller\Front;

use App\Entity\Option\AboutUs\AboutUs;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AboutUsController extends AbstractController
{
    /**
     * @param EntityManagerInterface $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/about-us", name="front_about_us")
     */
    public function aboutUs(EntityManagerInterface $entityManager)
    {
        return $this->render('front/controllers/about_us/index.html.twig', [
            'result' => $entityManager->getRepository(AboutUs::class)->findLastOne()
        ]);
    }
}