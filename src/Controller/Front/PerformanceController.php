<?php

namespace App\Controller\Front;

use App\Entity\Charts\RMBC\RiskMeasuresBenchmarkComparison;
use App\Entity\Option\Performance\PerformanceContentOption;
use App\Entity\Option\Performance\PerformanceOption;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class PerformanceController extends AbstractController
{
    /**
     * @Route("/performance", name="front_performance_index")
     */
    public function index(CsrfTokenManagerInterface $tokenManager, AuthenticationUtils $authenticationUtils, EntityManagerInterface $entityManager)
    {
        return $this->render('front/controllers/performance/index.html.twig', [
            'csrf_token'     => $tokenManager->getToken('authenticate')->getValue(),
            'error'          => $authenticationUtils->getLastAuthenticationError(),
            'lastUsername'   => $authenticationUtils->getLastUsername(),
            'rmbc'           => $entityManager->getRepository(RiskMeasuresBenchmarkComparison::class)->findOneBy([]),
            'pOption'        => $entityManager->getRepository(PerformanceOption::class)->getPerformance(),
            'pContentOption' => $entityManager->getRepository(PerformanceContentOption::class)->getNotHideContents()
        ]);
    }
}