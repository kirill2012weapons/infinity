<?php

namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class CustomSecurityController extends AbstractController
{
    /**
     * @Route("/finityx/login", name="finityx_login")
     */
    public function loginAction(AuthenticationUtils $authenticationUtils, Session $session)
    {
        $error        = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        $session->getFlashBag()->add('error', $error);
        $session->getFlashBag()->add('lastUsername', $lastUsername);

        return $this->redirectToRoute('front_performance_index');
    }
}