<?php

namespace App\Model\Chart\PBC;

class TypeNames
{
    protected $spxtrName;

    protected $efaName;

    protected $vtName;

    protected $consolidatedName;

    protected $spxtrColor;

    protected $efaColor;

    protected $vtColor;

    protected $consolidatedColor;

    /**
     * @return mixed
     */
    public function getSpxtrColor()
    {
        return $this->spxtrColor;
    }

    /**
     * @param mixed $spxtrColor
     */
    public function setSpxtrColor($spxtrColor): void
    {
        $this->spxtrColor = $spxtrColor;
    }

    /**
     * @return mixed
     */
    public function getEfaColor()
    {
        return $this->efaColor;
    }

    /**
     * @param mixed $efaColor
     */
    public function setEfaColor($efaColor): void
    {
        $this->efaColor = $efaColor;
    }

    /**
     * @return mixed
     */
    public function getVtColor()
    {
        return $this->vtColor;
    }

    /**
     * @param mixed $vtColor
     */
    public function setVtColor($vtColor): void
    {
        $this->vtColor = $vtColor;
    }

    /**
     * @return mixed
     */
    public function getConsolidatedColor()
    {
        return $this->consolidatedColor;
    }

    /**
     * @param mixed $consolidatedColor
     */
    public function setConsolidatedColor($consolidatedColor): void
    {
        $this->consolidatedColor = $consolidatedColor;
    }

    /**
     * @return mixed
     */
    public function getSpxtrName()
    {
        return $this->spxtrName;
    }

    /**
     * @param mixed $spxtrName
     *
     * @return self
     */
    public function setSpxtrName($spxtrName): self
    {
        $this->spxtrName = $spxtrName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEfaName()
    {
        return $this->efaName;
    }

    /**
     * @param mixed $efaName
     *
     * @return self
     */
    public function setEfaName($efaName): self
    {
        $this->efaName = $efaName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVtName()
    {
        return $this->vtName;
    }

    /**
     * @param mixed $vtName
     *
     * @return self
     */
    public function setVtName($vtName): self
    {
        $this->vtName = $vtName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConsolidatedName()
    {
        return $this->consolidatedName;
    }

    /**
     * @param mixed $consolidatedName
     *
     * @return self
     */
    public function setConsolidatedName($consolidatedName): self
    {
        $this->consolidatedName = $consolidatedName;

        return $this;
    }
}