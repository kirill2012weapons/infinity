<?php

namespace App\Model\Chart\RMBC;

class TypeNames
{
    protected $spxtrName;

    protected $efaName;

    protected $vtName;

    protected $consolidatedName;

    /**
     * @return mixed
     */
    public function getSpxtrName()
    {
        return $this->spxtrName;
    }

    /**
     * @param mixed $spxtrName
     *
     * @return self
     */
    public function setSpxtrName($spxtrName): self
    {
        $this->spxtrName = $spxtrName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEfaName()
    {
        return $this->efaName;
    }

    /**
     * @param mixed $efaName
     *
     * @return self
     */
    public function setEfaName($efaName): self
    {
        $this->efaName = $efaName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVtName()
    {
        return $this->vtName;
    }

    /**
     * @param mixed $vtName
     *
     * @return self
     */
    public function setVtName($vtName): self
    {
        $this->vtName = $vtName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConsolidatedName()
    {
        return $this->consolidatedName;
    }

    /**
     * @param mixed $consolidatedName
     *
     * @return self
     */
    public function setConsolidatedName($consolidatedName): self
    {
        $this->consolidatedName = $consolidatedName;

        return $this;
    }
}