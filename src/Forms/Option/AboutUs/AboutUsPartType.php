<?php

namespace App\Forms\Option\AboutUs;

use App\Entity\Option\AboutUs\AboutUsPart;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class AboutUsPartType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'required'    => false,
                'constraints' => [
                    new NotBlank(),
                ]
            ])
            ->add('description', TextType::class, [
                'required'    => false,
                'constraints' => [
                    new NotBlank(),
                ]
            ])
            ->add('image', FileType::class, [
                'data_class'  => null,
                'required'    => false,
                'constraints' => [
                    new \Symfony\Component\Validator\Constraints\File([
                        'maxSize' => '20024k',
                        'mimeTypes' => ['image/svg+xml', 'image/jpeg', 'image/png', 'text/html']
                    ]),
                    new NotBlank(),
                ]
            ])
        ;

        $builder->get('image')->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            if (null === $event->getData()) {
                /** @var File $file */
                $file = $event->getForm()->getData();
                $event->setData($file);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AboutUsPart::class,
            'label'      => 'About Us Part Option',
        ]);
    }

    public function getBlockPrefix()
    {
        return 'AboutUsPartType';
    }
}