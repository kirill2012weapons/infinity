<?php

namespace App\Forms\Option\AboutUs;

use App\Entity\Option\AboutUs\AboutUs;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AboutUsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('whiteTitle', TextType::class, [
                'required'  => false,
            ])
            ->add('blueTitle', TextType::class, [
                'required'  => false,
            ])
            ->add('content', TextareaType::class, [
                'required'  => false,
                'attr'      => [
                    'rows'  => 10
                ]
            ])
            ->add('aboutUsParts', CollectionType::class, [
                'entry_type'   => AboutUsPartType::class,
                'required'     => false,
                'allow_add'    => true,
                'prototype'    => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => false,
                'attr'         => [
                    'class'    => 'parts-collection',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AboutUs::class,
            'label'      => 'About Us Option',
        ]);
    }
}