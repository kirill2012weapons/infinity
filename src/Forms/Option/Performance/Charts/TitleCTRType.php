<?php

namespace App\Forms\Option\Performance\Charts;

use App\Entity\Option\Performance\PerformanceOption;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TitleCTRType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titleCTR', TextType::class, [
                'required'  => false,
                'label'     => 'Title'
            ])
            ->add('activeCTR', CheckboxType::class, [
                'label'    => 'Display',
                'required' => false,
                'attr'     => [
                    'class' => 'checkbox-custom'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PerformanceOption::class,
            'label'      => 'Chart Title',
        ]);
    }
}