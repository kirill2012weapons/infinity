<?php

namespace App\Forms\Option\Performance;

use App\Entity\Option\Performance\PerformanceOption;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PerformanceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('performanceContents', CollectionType::class, [
                'entry_type'   => PerformanceContentType::class,
                'required'     => false,
                'allow_add'    => true,
                'prototype'    => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label'        => false,
                'attr'         => [
                    'class'    => 'parts-collection',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PerformanceOption::class,
            'label'      => 'Performance',
        ]);
    }
}