<?php

namespace App\Forms\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class PeriodTransformer implements DataTransformerInterface
{

    public function transform($value)
    {
        if (!$value) return [
            'period_integer' => 0,
            'period_persent' => 0
        ];

        $arr         = explode(' ', $value);
        $firstValue  = $arr[0];
        $secondValue = trim($arr[1], ' \t\n\r\(\)');

        return [
            'period_integer' => $firstValue,
            'period_persent' => $secondValue
        ];
    }

    public function reverseTransform($value)
    {
        return $value['period_integer'] . ' (' . $value['period_persent'] . ')';
    }
}