<?php

namespace App\Forms\Chart\PBAC;

use App\Entity\Charts\PBAC\PerformanceByAssetClass;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

class PerformanceByAssetClassType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', TextType::class, [
                'required'  => false,
                'attr'      => [
                    'class' => 'js-datepicker'
                ],
            ])
                ->add('equities', NumberType::class, [
                'required'  => false,
            ])
            ->add('realEstate', NumberType::class, [
                'required'  => false,
            ])
            ->add('cash', NumberType::class, [
                'required'  => false,
            ])
        ;

        $builder->get('date')
            ->addModelTransformer(new CallbackTransformer(
                function ($data) {
                    return $data == null ? '' : $data->format('M Y');
                },
                function ($data) {
                    return \DateTime::createFromFormat('M Y', $data);
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PerformanceByAssetClass::class,
            'label'      => ''
        ]);
    }
}