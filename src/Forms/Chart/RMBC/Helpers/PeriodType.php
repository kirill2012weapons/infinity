<?php

namespace App\Forms\Chart\RMBC\Helpers;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class PeriodType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('period_integer', NumberType::class, [
                'required'     => false,
                'constraints'  => [
                    new NotBlank(),
                    new Range([
                        'min' => -1000,
                        'max' => 1000
                    ])
                ],
                'attr'         => [
                    'class'    => 'form-control'
                ]
            ])
            ->add('period_persent', NumberType::class, [
                'required'     => false,
                'constraints'  => [
                    new NotBlank(),
                    new Range([
                        'min' => 0,
                        'max' => 100
                    ])
                ],
                'attr'         => [
                    'class'    => 'form-control'
                ]
            ]);
    }
}