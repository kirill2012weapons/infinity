<?php

namespace App\Forms\Chart\RMBC;

use App\Forms\Chart\RMBC\Helpers\PeriodType;
use App\Forms\DataTransformer\PeriodTransformer;
use Symfony\Component\Form\AbstractType as SyAbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use App\Entity\Charts\RMBC\AbstractType as EntityAbstractType;
use Symfony\Component\Validator\Constraints\NotBlank;

class AbstractType extends SyAbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('endingVami', NumberType::class, [
                'required'     => false,
                'constraints'  => [
                ],
                'attr'         => [
                    'class'    => 'form-control'
                ]
            ])
            ->add('maxDrawdown', NumberType::class, [
                'required'     => false,
                'constraints'  => [
                ],
                'attr'         => [
                    'class'    => 'form-control'
                ]
            ])
            ->add('peakToValley', TextType::class, [
                'required'     => false,
                'constraints'  => [
                ],
                'attr'         => [
                    'class'    => 'form-control'
                ]
            ])
            ->add('recovery', ChoiceType::class, [
                'choices'      => [
                    'Ongoing'  => EntityAbstractType::RECOVERY_TYPE_ONGOING,
                    'Unstable' => EntityAbstractType::RECOVERY_TYPE_UNSTABLE,
                ],
                'required'     => false,
                'constraints'  => [
                    new NotBlank()
                ],
                'attr'         => [
                    'class'    => 'form-control'
                ]
            ])
            ->add('sharpeRatio', NumberType::class, [
                'required'     => false,
                'constraints'  => [
                ],
                'attr'         => [
                    'class'    => 'form-control'
                ]
            ])
            ->add('sortinoRatio', NumberType::class, [
                'required'     => false,
                'constraints'  => [
                ],
                'attr'         => [
                    'class'    => 'form-control'
                ]
            ])
            ->add('standardDeviation', NumberType::class, [
                'required'      => false,
                'constraints'   => [
                ],
                'attr'          => [
                    'class'     => 'form-control'
                ]
            ])
            ->add('downsideDeviation', NumberType::class, [
                'required'     => false,
                'constraints'  => [
                ],
                'attr'         => [
                    'class'    => 'form-control'
                ]
            ])
            ->add('correlation', NumberType::class, [
                'required'     => false,
                'constraints'  => [
                ],
                'attr'         => [
                    'class'    => 'form-control'
                ]
            ])
            ->add('beta', NumberType::class, [
                'required'     => false,
                'constraints'  => [
                ],
                'attr'         => [
                    'class'    => 'form-control'
                ]
            ])
            ->add('alpha', NumberType::class, [
                'required'     => false,
                'constraints'  => [
                ],
                'attr'         => [
                    'class'    => 'form-control'
                ]
            ])
            ->add('meanReturn', NumberType::class, [
                'required'     => false,
                'constraints'  => [
                ],
                'attr'         => [
                    'class'    => 'form-control'
                ]
            ])
            ->add('positivePeriods', PeriodType::class, [
                'required'     => false,
                'attr'         => [
                    'class'    => 'form-control'
                ]
            ])
            ->add('negativePeriods', PeriodType::class, [
                'required'     => false,
                'constraints'  => [
                ],
                'attr'         => [
                    'class'    => 'form-control'
                ]
            ])
            ->add('showItem', CheckboxType::class, [
                'label'             => 'Show',
                'required'          => false,
                'attr'              => [
                    'class'         => 'form-control'
                ]
            ])
        ;

        $builder->get('positivePeriods')
            ->addModelTransformer(new PeriodTransformer());

        $builder->get('negativePeriods')
            ->addModelTransformer(new PeriodTransformer());
    }
}