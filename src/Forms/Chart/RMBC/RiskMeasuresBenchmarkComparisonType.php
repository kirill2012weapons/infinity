<?php

namespace App\Forms\Chart\RMBC;

use App\Entity\Charts\RMBC\RiskMeasuresBenchmarkComparison;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RiskMeasuresBenchmarkComparisonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('spxtr', SpxtrType::class, [
                'required'  => false,
            ])
            ->add('efa', EfaType::class, [
                'required'  => false,
            ])
            ->add('vt', VtType::class, [
                'required'  => false,
            ])
            ->add('consolidated', ConsolidatedType::class, [
                'required'  => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RiskMeasuresBenchmarkComparison::class,
            'label'      => ''
        ]);
    }
}