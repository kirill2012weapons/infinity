<?php

namespace App\Forms\Chart\CTR;

use App\Entity\Charts\CTR\ContributionToReturn;
use App\Entity\Charts\CTR\Sector;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContributionToReturnType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sector', EntityType::class, [
                'required' => false,
                'class'    => Sector::class,
            ])
            ->add('accountContributionToReturn', NumberType::class, [
                'required'  => false,
            ])
            ->add('bmContributionToReturn', NumberType::class, [
                'required'  => false,
            ])
            ->add('contributionToReturnDifference', NumberType::class, [
                'required'  => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ContributionToReturn::class,
            'label'      => '',
        ]);
    }
}