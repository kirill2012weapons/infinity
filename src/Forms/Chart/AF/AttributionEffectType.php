<?php

namespace App\Forms\Chart\AF;

use App\Entity\Charts\AF\AttributionEffect;
use App\Entity\Charts\AF\Sector;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AttributionEffectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sector', EntityType::class, [
                'required' => false,
                'class'    => Sector::class,
            ])
            ->add('allocation', NumberType::class, [
                'required'  => false,
            ])
            ->add('selection', NumberType::class, [
                'required'  => false,
            ])
            ->add('total', NumberType::class, [
                'required'  => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AttributionEffect::class,
            'label'      => '',
        ]);
    }
}