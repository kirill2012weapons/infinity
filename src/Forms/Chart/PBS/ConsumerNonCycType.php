<?php

namespace App\Forms\Chart\PBS;

use App\Entity\Charts\PBS\ConsumerCyc;
use App\Entity\Charts\PBS\ConsumerNonCyc;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConsumerNonCycType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('slug', TextType::class, [
                'required'  => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ConsumerNonCyc::class,
            'label'      => 'Sector'
        ]);
    }
}