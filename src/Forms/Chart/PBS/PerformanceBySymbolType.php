<?php

namespace App\Forms\Chart\PBS;

use App\Entity\Charts\PBS\ConsumerCyc;
use App\Entity\Charts\PBS\ConsumerNonCyc;
use App\Entity\Charts\PBS\PerformanceBySymbol;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PerformanceBySymbolType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('symbol', TextType::class, [
                'required'  => false,
            ])
            ->add('description', TextareaType::class, [
                'required'  => false,
            ])
            ->add('avgWeight', NumberType::class, [
                'required'  => false,
            ])
            ->add('returnValue', NumberType::class, [
                'required'  => false,
            ])
            ->add('contribution', NumberType::class, [
                'required'  => false,
            ])
            ->add('unrealizedPL', NumberType::class, [
                'required'  => false,
            ])
            ->add('realizedPL', NumberType::class, [
                'required'  => false,
            ])
            ->add('open', ChoiceType::class, [
                'required'  => false,
                'choices'  => [
                    'Yes' => PerformanceBySymbol::STATE_OPEN,
                    'No' => PerformanceBySymbol::STATE_CLOSED,
                ],
            ])
            ->add('consumerCyc', EntityType::class, [
                'required'  => false,
                'class' => ConsumerCyc::class,
            ])
            ->add('consumerNonCyc', EntityType::class, [
                'required'  => false,
                'class' => ConsumerNonCyc::class,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PerformanceBySymbol::class,
            'label'      => 'Financial Instrument'
        ]);
    }
}