<?php

namespace App\Forms\Chart\PBC;

use App\Entity\Charts\PBC\Spxtr;
use App\Manager\Chart\PBC\NameManager;
use App\Model\Chart\PBC\TypeNames;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SpxtrType extends AbstractType
{
    /**
     * @var NameManager
     */
    protected $nameManager;

    public function __construct(
        NameManager $nameManager
    ) {
        $this->nameManager = $nameManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var TypeNames $name */
        $name = $this->nameManager->getNames();

        $options['children_label'] = $name->getSpxtrName();

        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Spxtr::class,
        ]);
    }
}