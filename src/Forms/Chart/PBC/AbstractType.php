<?php

namespace App\Forms\Chart\PBC;

use Symfony\Component\Form\AbstractType as SyAbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints as Assert;

class AbstractType extends SyAbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('returnValue', NumberType::class, [
                'required'    => false,
                'constraints' => [
                    new NotBlank(),
                    new Assert\Type('float')
                ],
                'label' => $options['children_label'],
            ]);
    }
}