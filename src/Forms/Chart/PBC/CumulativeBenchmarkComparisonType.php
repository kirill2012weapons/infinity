<?php

namespace App\Forms\Chart\PBC;

use App\Entity\Charts\PBC\CumulativeBenchmarkComparison;
use App\Manager\Chart\PBC\NameManager;
use App\Model\Chart\PBC\TypeNames;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

class CumulativeBenchmarkComparisonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', TextType::class, [
                'required'  => false,
            ])
            ->add('spxtr', SpxtrType::class, [
                'required'  => false,
            ])
            ->add('efa', EfaType::class, [
                'required'  => false,
            ])
            ->add('vt', VtType::class, [
                'required'  => false,
            ])
            ->add('consolidated', ConsolidatedType::class, [
                'required'  => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CumulativeBenchmarkComparison::class,
            'label'      => ''
        ]);
    }
}