<?php

namespace App\Forms\Chart\PBC;

use App\Model\Chart\PBC\TypeNames;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TypeNamesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('spxtrName', TextType::class, [
                'required'  => false,
            ])
            ->add('spxtrColor', TextType::class, [
                'required'  => false,
            ])
            ->add('efaName', TextType::class, [
                'required'  => false,
            ])
            ->add('efaColor', TextType::class, [
                'required'  => false,
            ])
            ->add('vtName', TextType::class, [
                'required'  => false,
            ])
            ->add('vtColor', TextType::class, [
                'required'  => false,
            ])
            ->add('consolidatedName', TextType::class, [
                'required'  => false,
            ])
            ->add('consolidatedColor', TextType::class, [
                'required'  => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TypeNames::class,
            'label'      => ''
        ]);
    }
}