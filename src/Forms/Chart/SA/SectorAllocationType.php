<?php

namespace App\Forms\Chart\SA;

use App\Entity\Charts\SA\Sector;
use App\Entity\Charts\SA\SectorAllocation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SectorAllocationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sector', EntityType::class, [
                'required' => false,
                'class'    => Sector::class,
            ])
            ->add('longWeight', NumberType::class, [
                'required'  => false,
            ])
            ->add('longParsedWeight', NumberType::class, [
                'required'  => false,
            ])
            ->add('shortWeight', NumberType::class, [
                'required'  => false,
            ])
            ->add('shortParsedWeight', NumberType::class, [
                'required'  => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SectorAllocation::class,
            'label'      => '',
        ]);
    }
}