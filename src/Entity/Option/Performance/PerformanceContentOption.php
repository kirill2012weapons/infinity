<?php

namespace App\Entity\Option\Performance;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="App\Repository\Option\Performance\PerformanceContentOptionRepository")
 * @ORM\Table(name="options_performance_content")
 */
class PerformanceContentOption
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $display = true;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content;

    /**
     * @var PerformanceOption
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Option\Performance\PerformanceOption", inversedBy="performanceContents", cascade={"persist"})
     * @ORM\JoinColumn(name="performance_id", referencedColumnName="id")
     */
    protected $performance;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDisplay(): ?bool
    {
        return $this->display;
    }

    public function setDisplay(?bool $display): self
    {
        $this->display = $display;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getPerformance(): ?PerformanceOption
    {
        return $this->performance;
    }

    public function setPerformance(?PerformanceOption $performance): self
    {
        $this->performance = $performance;

        return $this;
    }
}