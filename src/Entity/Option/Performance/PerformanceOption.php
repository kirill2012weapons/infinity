<?php

namespace App\Entity\Option\Performance;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Option\Performance\PerformanceOptionRepository")
 * @ORM\Table(name="options_performance")
 */
class PerformanceOption
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $titleCBC;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $activeCBC = true;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $titlePBC;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $activePBC = true;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $titleCTR;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $activeCTR = true;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $titleSA;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $activeSA = true;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $titleAF;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $activeAF = true;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $titlePBAC;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $activePBAC = true;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $activeRMBC = true;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    protected $activePBS = true;

    /**
     * @var PerformanceContentOption
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Option\Performance\PerformanceContentOption", mappedBy="performance", cascade={"persist", "remove"})
     */
    protected $performanceContents;

    public function __construct()
    {
        $this->performanceContents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitleCBC(): ?string
    {
        return $this->titleCBC;
    }

    public function setTitleCBC(?string $titleCBC): self
    {
        $this->titleCBC = $titleCBC;

        return $this;
    }

    public function getTitlePBC(): ?string
    {
        return $this->titlePBC;
    }

    public function setTitlePBC(?string $titlePBC): self
    {
        $this->titlePBC = $titlePBC;

        return $this;
    }

    public function getTitleCTR(): ?string
    {
        return $this->titleCTR;
    }

    public function setTitleCTR(?string $titleCTR): self
    {
        $this->titleCTR = $titleCTR;

        return $this;
    }

    public function getTitleSA(): ?string
    {
        return $this->titleSA;
    }

    public function setTitleSA(?string $titleSA): self
    {
        $this->titleSA = $titleSA;

        return $this;
    }

    public function getTitleAF(): ?string
    {
        return $this->titleAF;
    }

    public function setTitleAF(?string $titleAF): self
    {
        $this->titleAF = $titleAF;

        return $this;
    }

    public function getTitlePBAC(): ?string
    {
        return $this->titlePBAC;
    }

    public function setTitlePBAC(?string $titlePBAC): self
    {
        $this->titlePBAC = $titlePBAC;

        return $this;
    }

    /**
     * @return Collection|PerformanceContentOption[]
     */
    public function getPerformanceContents(): Collection
    {
        return $this->performanceContents;
    }

    public function addPerformanceContent(?PerformanceContentOption $performanceContent): self
    {
        if (!$this->performanceContents->contains($performanceContent)) {
            $this->performanceContents[] = $performanceContent;
            $performanceContent->setPerformance($this);
        }

        return $this;
    }

    public function removePerformanceContent(?PerformanceContentOption $performanceContent): self
    {
        if ($this->performanceContents->contains($performanceContent)) {
            $this->performanceContents->removeElement($performanceContent);
            // set the owning side to null (unless already changed)
            if ($performanceContent->getPerformance() === $this) {
                $performanceContent->setPerformance(null);
            }
        }

        return $this;
    }

    public function getActiveCBC(): ?bool
    {
        return $this->activeCBC;
    }

    public function setActiveCBC(?bool $activeCBC): self
    {
        $this->activeCBC = $activeCBC;

        return $this;
    }

    public function getActivePBC(): ?bool
    {
        return $this->activePBC;
    }

    public function setActivePBC(?bool $activePBC): self
    {
        $this->activePBC = $activePBC;

        return $this;
    }

    public function getActiveCTR(): ?bool
    {
        return $this->activeCTR;
    }

    public function setActiveCTR(?bool $activeCTR): self
    {
        $this->activeCTR = $activeCTR;

        return $this;
    }

    public function getActiveSA(): ?bool
    {
        return $this->activeSA;
    }

    public function setActiveSA(?bool $activeSA): self
    {
        $this->activeSA = $activeSA;

        return $this;
    }

    public function getActiveAF(): ?bool
    {
        return $this->activeAF;
    }

    public function setActiveAF(?bool $activeAF): self
    {
        $this->activeAF = $activeAF;

        return $this;
    }

    public function getActivePBAC(): ?bool
    {
        return $this->activePBAC;
    }

    public function setActivePBAC(?bool $activePBAC): self
    {
        $this->activePBAC = $activePBAC;

        return $this;
    }

    public function getActiveRMBC(): ?bool
    {
        return $this->activeRMBC;
    }

    public function setActiveRMBC(?bool $activeRMBC): self
    {
        $this->activeRMBC = $activeRMBC;

        return $this;
    }

    public function getActivePBS(): ?bool
    {
        return $this->activePBS;
    }

    public function setActivePBS(?bool $activePBS): self
    {
        $this->activePBS = $activePBS;

        return $this;
    }
}