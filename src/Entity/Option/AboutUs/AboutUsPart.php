<?php

namespace App\Entity\Option\AboutUs;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Option\AboutUs\AboutUsPartRepository")
 * @ORM\Table(name="options_about_us_part")
 */
class AboutUsPart
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Please, upload the IMAGE file.")
     * @Assert\File(mimeTypes={ "image/svg+xml", "image/jpeg", "image/png" })
     */
    protected $image;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    protected $description;

    /**
     * @var AboutUs
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Option\AboutUs\AboutUs", inversedBy="aboutUsParts")
     * @ORM\JoinColumn(name="about_us_id", referencedColumnName="id")
     */
    protected $aboutUs;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAboutUs(): ?AboutUs
    {
        return $this->aboutUs;
    }

    public function setAboutUs(?AboutUs $aboutUs): self
    {
        $this->aboutUs = $aboutUs;

        return $this;
    }
}