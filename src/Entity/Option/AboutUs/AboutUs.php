<?php

namespace App\Entity\Option\AboutUs;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Option\AboutUs\AboutUsRepository")
 * @ORM\Table(name="options_about_us")
 */
class AboutUs
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    protected $whiteTitle;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    protected $blueTitle;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     */
    protected $content;

    /**
     * @var AboutUsPart[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Option\AboutUs\AboutUsPart", mappedBy="aboutUs", cascade={"persist", "remove"})
     */
    protected $aboutUsParts;

    public function __construct()
    {
        $this->aboutUsParts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWhiteTitle(): ?string
    {
        return $this->whiteTitle;
    }

    public function setWhiteTitle(string $whiteTitle): self
    {
        $this->whiteTitle = $whiteTitle;

        return $this;
    }

    public function getBlueTitle(): ?string
    {
        return $this->blueTitle;
    }

    public function setBlueTitle(string $blueTitle): self
    {
        $this->blueTitle = $blueTitle;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return ArrayCollection|AboutUsPart[]
     */
    public function getAboutUsParts()
    {
        return $this->aboutUsParts;
    }

    public function addAboutUsPart(AboutUsPart $aboutUsPart): self
    {
        if (!$this->aboutUsParts->contains($aboutUsPart)) {
            $this->aboutUsParts[] = $aboutUsPart;
            $aboutUsPart->setAboutUs($this);
        }

        return $this;
    }

    public function removeAboutUsPart(AboutUsPart $aboutUsPart): self
    {
        if ($this->aboutUsParts->contains($aboutUsPart)) {
            $this->aboutUsParts->removeElement($aboutUsPart);
            // set the owning side to null (unless already changed)
            if ($aboutUsPart->getAboutUs() === $this) {
                $aboutUsPart->setAboutUs(null);
            }
        }

        return $this;
    }
}