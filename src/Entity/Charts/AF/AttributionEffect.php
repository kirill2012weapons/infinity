<?php

namespace App\Entity\Charts\AF;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Charts\AF\AttributionEffectRepository")
 * @ORM\Table(name="af_attribution_effect")
 */
class AttributionEffect
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Sector
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Charts\AF\Sector", inversedBy="attributionEffect")
     * @ORM\JoinColumn(name="sector_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     */
    protected $sector;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=false)
     * @Assert\NotBlank
     */
    protected $allocation;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=false)
     * @Assert\NotBlank
     */
    protected $selection;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=false)
     * @Assert\NotBlank
     */
    protected $total;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAllocation(): ?float
    {
        return $this->allocation;
    }

    public function setAllocation(?float $allocation): self
    {
        $this->allocation = $allocation;

        return $this;
    }

    public function getSelection(): ?float
    {
        return $this->selection;
    }

    public function setSelection(?float $selection): self
    {
        $this->selection = $selection;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(?float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getSector(): ?Sector
    {
        return $this->sector;
    }

    public function setSector(?Sector $sector): self
    {
        $this->sector = $sector;

        return $this;
    }
}