<?php

namespace App\Entity\Charts\AF;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Charts\AF\SectorRepository")
 * @ORM\Table(name="af_sector")
 * @UniqueEntity("slug")
 */
class Sector
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=false)
     * @Assert\NotBlank
     */
    protected $slug;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Assert\NotBlank
     */
    protected $color;

    /**
     * @var AttributionEffect[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Charts\AF\AttributionEffect", mappedBy="sector", cascade={"remove"})
     */
    protected $attributionEffect;

    public function __construct()
    {
        $this->attributionEffect = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->slug;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return ArrayCollection|AttributionEffect[]
     */
    public function getAttributionEffect(): ArrayCollection
    {
        return $this->attributionEffect;
    }

    public function addAttributionEffect(AttributionEffect $attributionEffect): self
    {
        if (!$this->attributionEffect->contains($attributionEffect)) {
            $this->attributionEffect[] = $attributionEffect;
            $attributionEffect->setSector($this);
        }

        return $this;
    }

    public function removeAttributionEffect(AttributionEffect $attributionEffect): self
    {
        if ($this->attributionEffect->contains($attributionEffect)) {
            $this->attributionEffect->removeElement($attributionEffect);
            // set the owning side to null (unless already changed)
            if ($attributionEffect->getSector() === $this) {
                $attributionEffect->setSector(null);
            }
        }

        return $this;
    }
}