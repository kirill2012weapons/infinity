<?php

namespace App\Entity\Charts\PBAC;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Charts\PBAC\PerformanceByAssetClassRepository")
 * @ORM\Table(name="pbac_performance_by_asset_class")
 */
class PerformanceByAssetClass
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     * @Assert\NotBlank
     */
    protected $date;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=false)
     * @Assert\NotBlank
     */
    protected $equities;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=false)
     * @Assert\NotBlank
     */
    protected $realEstate;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=false)
     * @Assert\NotBlank
     */
    protected $cash;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getEquities(): ?float
    {
        return $this->equities;
    }

    public function setEquities(?float $equities): self
    {
        $this->equities = $equities;

        return $this;
    }

    public function getRealEstate(): ?float
    {
        return $this->realEstate;
    }

    public function setRealEstate(?float $realEstate): self
    {
        $this->realEstate = $realEstate;

        return $this;
    }

    public function getCash(): ?float
    {
        return $this->cash;
    }

    public function setCash(?float $cash): self
    {
        $this->cash = $cash;

        return $this;
    }
}