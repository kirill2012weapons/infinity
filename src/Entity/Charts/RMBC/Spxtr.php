<?php

namespace App\Entity\Charts\RMBC;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="rmbc_spxtr")
 */
class Spxtr extends AbstractType
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    protected $type = parent::SPXTR_TYPE;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Charts\RMBC\RiskMeasuresBenchmarkComparison", inversedBy="spxtr")
     * @ORM\JoinColumn(name="rmbc_id", referencedColumnName="id")
     */
    protected $riskMeasuresBenchmarkComparison;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getRiskMeasuresBenchmarkComparison(): ?RiskMeasuresBenchmarkComparison
    {
        return $this->riskMeasuresBenchmarkComparison;
    }

    public function setRiskMeasuresBenchmarkComparison(?RiskMeasuresBenchmarkComparison $riskMeasuresBenchmarkComparison): self
    {
        $this->riskMeasuresBenchmarkComparison = $riskMeasuresBenchmarkComparison;

        return $this;
    }
}