<?php

namespace App\Entity\Charts\RMBC;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass
 */
class AbstractType
{
    public const CONSOLIDATED_TYPE = 'consolidated';
    public const EFA_TYPE          = 'efa';
    public const SPXTR_TYPE        = 'spxtr';
    public const VT_TYPE           = 'vt';

    public const RECOVERY_TYPE_ONGOING  = 'ongoing';
    public const RECOVERY_TYPE_UNSTABLE = 'unstable';

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=true)
     */
    protected $endingVami;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=true)
     */
    protected $maxDrawdown;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $peakToValley;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $recovery;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=true)
     */
    protected $sharpeRatio;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=true)
     */
    protected $sortinoRatio;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=true)
     */
    protected $standardDeviation;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=true)
     */
    protected $downsideDeviation;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=true)
     */
    protected $correlation;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=true)
     */
    protected $beta;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=true)
     */
    protected $alpha;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=true)
     */
    protected $meanReturn;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $positivePeriods;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $negativePeriods;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    protected $showItem = true;

    public function getShowItem(): ?bool
    {
        return $this->showItem;
    }

    public function setShowItem(?bool $show): self
    {
        $this->showItem = $show;

        return $this;
    }

    public function getEndingVami(): ?float
    {
        return $this->endingVami;
    }

    public function setEndingVami(?float $endingVami): self
    {
        $this->endingVami = $endingVami;

        return $this;
    }

    public function getMaxDrawdown(): ?float
    {
        return $this->maxDrawdown;
    }

    public function setMaxDrawdown(?float $maxDrawdown): self
    {
        $this->maxDrawdown = $maxDrawdown;

        return $this;
    }

    public function getPeakToValley(): ?string
    {
        return $this->peakToValley;
    }

    public function setPeakToValley(?string $peakToValley): self
    {
        $this->peakToValley = $peakToValley;

        return $this;
    }

    public function getRecovery(): ?string
    {
        return $this->recovery;
    }

    public function setRecovery(?string $recovery): self
    {
        $this->recovery = $recovery;

        return $this;
    }

    public function getSharpeRatio(): ?float
    {
        return $this->sharpeRatio;
    }

    public function setSharpeRatio(?float $sharpeRatio): self
    {
        $this->sharpeRatio = $sharpeRatio;

        return $this;
    }

    public function getSortinoRatio(): ?float
    {
        return $this->sortinoRatio;
    }

    public function setSortinoRatio(?float $sortinoRatio): self
    {
        $this->sortinoRatio = $sortinoRatio;

        return $this;
    }

    public function getStandardDeviation(): ?float
    {
        return $this->standardDeviation;
    }

    public function setStandardDeviation(?float $standardDeviation): self
    {
        $this->standardDeviation = $standardDeviation;

        return $this;
    }

    public function getDownsideDeviation(): ?float
    {
        return $this->downsideDeviation;
    }

    public function setDownsideDeviation(?float $downsideDeviation): self
    {
        $this->downsideDeviation = $downsideDeviation;

        return $this;
    }

    public function getCorrelation(): ?float
    {
        return $this->correlation;
    }

    public function setCorrelation(?float $correlation): self
    {
        $this->correlation = $correlation;

        return $this;
    }

    public function getBeta(): ?float
    {
        return $this->beta;
    }

    public function setBeta(?float $beta): self
    {
        $this->beta = $beta;

        return $this;
    }

    public function getAlpha(): ?float
    {
        return $this->alpha;
    }

    public function setAlpha(?float $alpha): self
    {
        $this->alpha = $alpha;

        return $this;
    }

    public function getMeanReturn(): ?float
    {
        return $this->meanReturn;
    }

    public function setMeanReturn(?float $meanReturn): self
    {
        $this->meanReturn = $meanReturn;

        return $this;
    }

    public function getPositivePeriods(): ?string
    {
        return $this->positivePeriods;
    }

    public function setPositivePeriods(?string $positivePeriods): self
    {
        $this->positivePeriods = $positivePeriods;

        return $this;
    }

    public function getNegativePeriods(): ?string
    {
        return $this->negativePeriods;
    }

    public function setNegativePeriods(?string $negativePeriods): self
    {
        $this->negativePeriods = $negativePeriods;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }
}