<?php

namespace App\Entity\Charts\RMBC;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Charts\RMBC\RiskMeasuresBenchmarkComparisonRepository")
 * @ORM\Table(name="rmbc_risk_measures_benchmark_comparison")
 */
class RiskMeasuresBenchmarkComparison
{
    public const TYPE_CHART = 'data';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    protected $dataType = self::TYPE_CHART;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Charts\RMBC\Spxtr", mappedBy="riskMeasuresBenchmarkComparison", cascade={"persist", "remove"})
     * @Assert\NotBlank
     */
    protected $spxtr;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Charts\RMBC\Efa", mappedBy="riskMeasuresBenchmarkComparison", cascade={"persist", "remove"})
     * @Assert\NotBlank
     */
    protected $efa;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Charts\RMBC\Vt", mappedBy="riskMeasuresBenchmarkComparison", cascade={"persist", "remove"})
     * @Assert\NotBlank
     */
    protected $vt;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Charts\RMBC\Consolidated", mappedBy="riskMeasuresBenchmarkComparison", cascade={"persist", "remove"})
     * @Assert\NotBlank
     */
    protected $consolidated;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDataType(): ?string
    {
        return $this->dataType;
    }

    public function setDataType(?string $dataType): self
    {
        $this->dataType = $dataType;

        return $this;
    }

    public function getSpxtr(): ?Spxtr
    {
        return $this->spxtr;
    }

    public function setSpxtr(?Spxtr $spxtr): self
    {
        $this->spxtr = $spxtr;

        // set (or unset) the owning side of the relation if necessary
        $newCumulativeBenchmarkComparison = null === $spxtr ? null : $this;
        if ($spxtr->getRiskMeasuresBenchmarkComparison() !== $newCumulativeBenchmarkComparison) {
            $spxtr->setRiskMeasuresBenchmarkComparison($newCumulativeBenchmarkComparison);
        }

        return $this;
    }

    public function getEfa(): ?Efa
    {
        return $this->efa;
    }

    public function setEfa(?Efa $efa): self
    {
        $this->efa = $efa;

        // set (or unset) the owning side of the relation if necessary
        $newCumulativeBenchmarkComparison = null === $efa ? null : $this;
        if ($efa->getRiskMeasuresBenchmarkComparison() !== $newCumulativeBenchmarkComparison) {
            $efa->setRiskMeasuresBenchmarkComparison($newCumulativeBenchmarkComparison);
        }

        return $this;
    }

    public function getVt(): ?Vt
    {
        return $this->vt;
    }

    public function setVt(?Vt $vt): self
    {
        $this->vt = $vt;

        // set (or unset) the owning side of the relation if necessary
        $newCumulativeBenchmarkComparison = null === $vt ? null : $this;
        if ($vt->getRiskMeasuresBenchmarkComparison() !== $newCumulativeBenchmarkComparison) {
            $vt->setRiskMeasuresBenchmarkComparison($newCumulativeBenchmarkComparison);
        }

        return $this;
    }

    public function getConsolidated(): ?Consolidated
    {
        return $this->consolidated;
    }

    public function setConsolidated(?Consolidated $consolidated): self
    {
        $this->consolidated = $consolidated;

        // set (or unset) the owning side of the relation if necessary
        $newCumulativeBenchmarkComparison = null === $consolidated ? null : $this;
        if ($consolidated->getRiskMeasuresBenchmarkComparison() !== $newCumulativeBenchmarkComparison) {
            $consolidated->setRiskMeasuresBenchmarkComparison($newCumulativeBenchmarkComparison);
        }

        return $this;
    }
}