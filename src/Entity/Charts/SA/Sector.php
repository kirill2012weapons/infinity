<?php

namespace App\Entity\Charts\SA;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Charts\SA\SectorRepository")
 * @ORM\Table(name="sa_sector")
 * @UniqueEntity("slug")
 */
class Sector
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=false)
     * @Assert\NotBlank
     */
    protected $slug;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Assert\NotBlank
     */
    protected $color;

    /**
     * @var SectorAllocation[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Charts\SA\SectorAllocation", mappedBy="sector", cascade={"remove"})
     */
    protected $sectorAllocation;

    public function __construct()
    {
        $this->sectorAllocation = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->slug;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return Collection|SectorAllocation[]
     */
    public function getSectorAllocation(): Collection
    {
        return $this->sectorAllocation;
    }

    public function addSectorAllocation(SectorAllocation $sectorAllocation): self
    {
        if (!$this->sectorAllocation->contains($sectorAllocation)) {
            $this->sectorAllocation[] = $sectorAllocation;
            $sectorAllocation->setSector($this);
        }

        return $this;
    }

    public function removeSectorAllocation(SectorAllocation $sectorAllocation): self
    {
        if ($this->sectorAllocation->contains($sectorAllocation)) {
            $this->sectorAllocation->removeElement($sectorAllocation);
            // set the owning side to null (unless already changed)
            if ($sectorAllocation->getSector() === $this) {
                $sectorAllocation->setSector(null);
            }
        }

        return $this;
    }
}