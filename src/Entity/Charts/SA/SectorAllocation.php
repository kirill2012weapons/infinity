<?php

namespace App\Entity\Charts\SA;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as AcmeAssert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Charts\SA\SectorAllocationRepository")
 * @ORM\Table(name="sa_sector_allocation")
 */
class SectorAllocation
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Sector
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Charts\SA\Sector", inversedBy="sectorAllocation")
     * @ORM\JoinColumn(name="sector_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     */
    protected $sector;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=false)
     * @Assert\NotBlank
     * @AcmeAssert\Chart\SA\ContainsMaximumInterest(field_name="longWeight", name_space_class="App\Entity\Charts\SA\SectorAllocation")
     */
    protected $longWeight;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=false)
     * @Assert\NotBlank
     * @AcmeAssert\Chart\SA\ContainsMaximumInterest(field_name="longParsedWeight", name_space_class="App\Entity\Charts\SA\SectorAllocation")
     */
    protected $longParsedWeight;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=false)
     * @Assert\NotBlank
     * @AcmeAssert\Chart\SA\ContainsMaximumInterest(field_name="shortWeight", name_space_class="App\Entity\Charts\SA\SectorAllocation")
     */
    protected $shortWeight;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=false)
     * @Assert\NotBlank
     * @AcmeAssert\Chart\SA\ContainsMaximumInterest(field_name="shortParsedWeight", name_space_class="App\Entity\Charts\SA\SectorAllocation")
     */
    protected $shortParsedWeight;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLongWeight(): ?float
    {
        return $this->longWeight;
    }

    public function setLongWeight(?float $longWeight): self
    {
        $this->longWeight = $longWeight;

        return $this;
    }

    public function getLongParsedWeight(): ?float
    {
        return $this->longParsedWeight;
    }

    public function setLongParsedWeight(?float $longParsedWeight): self
    {
        $this->longParsedWeight = $longParsedWeight;

        return $this;
    }

    public function getShortWeight(): ?float
    {
        return $this->shortWeight;
    }

    public function setShortWeight(?float $shortWeight): self
    {
        $this->shortWeight = $shortWeight;

        return $this;
    }

    public function getShortParsedWeight(): ?float
    {
        return $this->shortParsedWeight;
    }

    public function setShortParsedWeight(?float $shortParsedWeight): self
    {
        $this->shortParsedWeight = $shortParsedWeight;

        return $this;
    }

    public function getSector(): ?Sector
    {
        return $this->sector;
    }

    public function setSector(?Sector $sector): self
    {
        $this->sector = $sector;

        return $this;
    }
}