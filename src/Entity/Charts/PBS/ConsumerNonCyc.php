<?php

namespace App\Entity\Charts\PBS;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Charts\PBS\ConsumerNonCycRepository")
 * @ORM\Table(name="pbs_consumer_non_cyc")
 * @UniqueEntity("slug")
 */
class ConsumerNonCyc
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank
     */
    protected $slug;

    /**
     * @var PerformanceBySymbol[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Charts\PBS\PerformanceBySymbol", mappedBy="consumerNonCyc", cascade={"remove"})
     */
    protected $performanceBySymbol;

    public function __construct()
    {
        $this->performanceBySymbol = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->slug;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|PerformanceBySymbol[]
     */
    public function getPerformanceBySymbol(): Collection
    {
        return $this->performanceBySymbol;
    }

    public function addPerformanceBySymbol(PerformanceBySymbol $performanceBySymbol): self
    {
        if (!$this->performanceBySymbol->contains($performanceBySymbol)) {
            $this->performanceBySymbol[] = $performanceBySymbol;
            $performanceBySymbol->setConsumerNonCyc($this);
        }

        return $this;
    }

    public function removePerformanceBySymbol(PerformanceBySymbol $performanceBySymbol): self
    {
        if ($this->performanceBySymbol->contains($performanceBySymbol)) {
            $this->performanceBySymbol->removeElement($performanceBySymbol);
            // set the owning side to null (unless already changed)
            if ($performanceBySymbol->getConsumerNonCyc() === $this) {
                $performanceBySymbol->setConsumerNonCyc(null);
            }
        }

        return $this;
    }
}