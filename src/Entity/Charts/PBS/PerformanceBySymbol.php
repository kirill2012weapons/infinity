<?php

namespace App\Entity\Charts\PBS;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Charts\PBS\PerformanceBySymbolRepository")
 * @ORM\Table(name="pbs_performance_by_symbol")
 * @UniqueEntity("symbol")
 */
class PerformanceBySymbol
{
    public const STATE_OPEN   = 'open';
    public const STATE_CLOSED = 'closed';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=false)
     * @Assert\NotBlank
     */
    protected $symbol;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=false)
     * @Assert\NotBlank
     */
    protected $avgWeight;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=false)
     * @Assert\NotBlank
     */
    protected $returnValue;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=false)
     * @Assert\NotBlank
     */
    protected $contribution;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=false)
     * @Assert\NotBlank
     */
    protected $unrealizedPL;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=false)
     * @Assert\NotBlank
     */
    protected $realizedPL;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank
     */
    protected $open;

    /**
     * @var ConsumerCyc
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Charts\PBS\ConsumerCyc", inversedBy="performanceBySymbol")
     * @ORM\JoinColumn(name="consumer_cyc_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     */
    protected $consumerCyc;

    /**
     * @var ConsumerNonCyc
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Charts\PBS\ConsumerNonCyc", inversedBy="performanceBySymbol")
     * @ORM\JoinColumn(name="consumer_non_cyc_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     */
    protected $consumerNonCyc;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    public function setSymbol(?string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getConsumerCyc(): ?ConsumerCyc
    {
        return $this->consumerCyc;
    }

    public function setConsumerCyc(?ConsumerCyc $consumerCyc): self
    {
        $this->consumerCyc = $consumerCyc;

        return $this;
    }

    public function getConsumerNonCyc(): ?ConsumerNonCyc
    {
        return $this->consumerNonCyc;
    }

    public function setConsumerNonCyc(?ConsumerNonCyc $consumerNonCyc): self
    {
        $this->consumerNonCyc = $consumerNonCyc;

        return $this;
    }

    public function getAvgWeight(): ?float
    {
        return $this->avgWeight;
    }

    public function setAvgWeight(?float $avgWeight): self
    {
        $this->avgWeight = $avgWeight;

        return $this;
    }

    public function getReturnValue(): ?float
    {
        return $this->returnValue;
    }

    public function setReturnValue(?float $returnValue): self
    {
        $this->returnValue = $returnValue;

        return $this;
    }

    public function getContribution(): ?float
    {
        return $this->contribution;
    }

    public function setContribution(?float $contribution): self
    {
        $this->contribution = $contribution;

        return $this;
    }

    public function getUnrealizedPL(): ?float
    {
        return $this->unrealizedPL;
    }

    public function setUnrealizedPL(?float $unrealizedPL): self
    {
        $this->unrealizedPL = $unrealizedPL;

        return $this;
    }

    public function getRealizedPL(): ?float
    {
        return $this->realizedPL;
    }

    public function setRealizedPL(?float $realizedPL): self
    {
        $this->realizedPL = $realizedPL;

        return $this;
    }

    public function getOpen(): ?string
    {
        return $this->open;
    }

    public function setOpen(?string $open): self
    {
        $this->open = $open;

        return $this;
    }
}