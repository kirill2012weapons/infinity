<?php

namespace App\Entity\Charts\CTR;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Charts\CTR\ContributionToReturnRepository")
 * @ORM\Table(name="ctr_contribution_to_return")
 */
class ContributionToReturn
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Sector
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Charts\CTR\Sector", inversedBy="contributionToReturn")
     * @ORM\JoinColumn(name="sector_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     */
    protected $sector;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=false)
     * @Assert\NotBlank
     */
    protected $accountContributionToReturn;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=false)
     * @Assert\NotBlank
     */
    protected $bmContributionToReturn;

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT", nullable=false)
     * @Assert\NotBlank
     */
    protected $contributionToReturnDifference;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccountContributionToReturn(): ?float
    {
        return $this->accountContributionToReturn;
    }

    public function setAccountContributionToReturn(?float $accountContributionToReturn): self
    {
        $this->accountContributionToReturn = $accountContributionToReturn;

        return $this;
    }

    public function getBmContributionToReturn(): ?float
    {
        return $this->bmContributionToReturn;
    }

    public function setBmContributionToReturn(?float $bmContributionToReturn): self
    {
        $this->bmContributionToReturn = $bmContributionToReturn;

        return $this;
    }

    public function getContributionToReturnDifference(): ?float
    {
        return $this->contributionToReturnDifference;
    }

    public function setContributionToReturnDifference(?float $contributionToReturnDifference): self
    {
        $this->contributionToReturnDifference = $contributionToReturnDifference;

        return $this;
    }

    public function getSector(): ?Sector
    {
        return $this->sector;
    }

    public function setSector(?Sector $sector): self
    {
        $this->sector = $sector;

        return $this;
    }
}