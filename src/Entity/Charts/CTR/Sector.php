<?php

namespace App\Entity\Charts\CTR;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Charts\CTR\SectorRepository")
 * @ORM\Table(name="ctr_sector")
 * @UniqueEntity("slug")
 */
class Sector
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=false)
     * @Assert\NotBlank
     */
    protected $slug;

    /**
     * @var ContributionToReturn[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Charts\CTR\ContributionToReturn", mappedBy="sector", cascade={"remove"})
     */
    protected $contributionToReturn;

    public function __construct()
    {
        $this->contributionToReturn = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->slug;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|ContributionToReturn[]
     */
    public function getContributionToReturn(): Collection
    {
        return $this->contributionToReturn;
    }

    public function addContributionToReturn(ContributionToReturn $contributionToReturn): self
    {
        if (!$this->contributionToReturn->contains($contributionToReturn)) {
            $this->contributionToReturn[] = $contributionToReturn;
            $contributionToReturn->setSector($this);
        }

        return $this;
    }

    public function removeContributionToReturn(ContributionToReturn $contributionToReturn): self
    {
        if ($this->contributionToReturn->contains($contributionToReturn)) {
            $this->contributionToReturn->removeElement($contributionToReturn);
            // set the owning side to null (unless already changed)
            if ($contributionToReturn->getSector() === $this) {
                $contributionToReturn->setSector(null);
            }
        }

        return $this;
    }
}