<?php

namespace App\Entity\Charts\PBC;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Charts\PBC\CumulativeBenchmarkComparisonRepository")
 * @ORM\Table(name="pbc_cumulative_benchmark_comparison")
 */
class CumulativeBenchmarkComparison
{
    public const TYPE_CHART = 'data';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    protected $dataType = self::TYPE_CHART;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    protected $date;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Charts\PBC\Spxtr", mappedBy="cumulativeBenchmarkComparison", cascade={"persist", "remove"})
     * @Assert\NotBlank
     */
    protected $spxtr;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Charts\PBC\Efa", mappedBy="cumulativeBenchmarkComparison", cascade={"persist", "remove"})
     * @Assert\NotBlank
     */
    protected $efa;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Charts\PBC\Vt", mappedBy="cumulativeBenchmarkComparison", cascade={"persist", "remove"})
     * @Assert\NotBlank
     */
    protected $vt;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Charts\PBC\Consolidated", mappedBy="cumulativeBenchmarkComparison", cascade={"persist", "remove"})
     * @Assert\NotBlank
     */
    protected $consolidated;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDataType(): ?string
    {
        return $this->dataType;
    }

    public function setDataType(?string $dataType): self
    {
        $this->dataType = $dataType;

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getSpxtr(): ?Spxtr
    {
        return $this->spxtr;
    }

    public function setSpxtr(?Spxtr $spxtr): self
    {
        $this->spxtr = $spxtr;

        // set (or unset) the owning side of the relation if necessary
        $newCumulativeBenchmarkComparison = null === $spxtr ? null : $this;
        if ($spxtr->getCumulativeBenchmarkComparison() !== $newCumulativeBenchmarkComparison) {
            $spxtr->setCumulativeBenchmarkComparison($newCumulativeBenchmarkComparison);
        }

        return $this;
    }

    public function getEfa(): ?Efa
    {
        return $this->efa;
    }

    public function setEfa(?Efa $efa): self
    {
        $this->efa = $efa;

        // set (or unset) the owning side of the relation if necessary
        $newCumulativeBenchmarkComparison = null === $efa ? null : $this;
        if ($efa->getCumulativeBenchmarkComparison() !== $newCumulativeBenchmarkComparison) {
            $efa->setCumulativeBenchmarkComparison($newCumulativeBenchmarkComparison);
        }

        return $this;
    }

    public function getVt(): ?Vt
    {
        return $this->vt;
    }

    public function setVt(?Vt $vt): self
    {
        $this->vt = $vt;

        // set (or unset) the owning side of the relation if necessary
        $newCumulativeBenchmarkComparison = null === $vt ? null : $this;
        if ($vt->getCumulativeBenchmarkComparison() !== $newCumulativeBenchmarkComparison) {
            $vt->setCumulativeBenchmarkComparison($newCumulativeBenchmarkComparison);
        }

        return $this;
    }

    public function getConsolidated(): ?Consolidated
    {
        return $this->consolidated;
    }

    public function setConsolidated(?Consolidated $consolidated): self
    {
        $this->consolidated = $consolidated;

        // set (or unset) the owning side of the relation if necessary
        $newCumulativeBenchmarkComparison = null === $consolidated ? null : $this;
        if ($consolidated->getCumulativeBenchmarkComparison() !== $newCumulativeBenchmarkComparison) {
            $consolidated->setCumulativeBenchmarkComparison($newCumulativeBenchmarkComparison);
        }

        return $this;
    }
}