<?php

namespace App\Entity\Charts\PBC;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass
 */
class AbstractType
{
    public const CONSOLIDATED_TYPE = 'consolidated';
    public const EFA_TYPE          = 'efa';
    public const SPXTR_TYPE        = 'spxtr';
    public const VT_TYPE           = 'vt';

    /**
     * @var float
     *
     * @ORM\Column(type="float", columnDefinition="FLOAT")
     * @Assert\NotBlank
     */
    protected $returnValue;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $color;

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getReturnValue(): ?float
    {
        return $this->returnValue;
    }

    public function setReturnValue(?float $returnValue): self
    {
        $this->returnValue = $returnValue;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }
}