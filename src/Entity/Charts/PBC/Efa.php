<?php

namespace App\Entity\Charts\PBC;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="pbc_efa")
 */
class Efa extends AbstractType
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    protected $type = parent::EFA_TYPE;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Charts\PBC\CumulativeBenchmarkComparison", inversedBy="spxtr")
     * @ORM\JoinColumn(name="cbc_id", referencedColumnName="id")
     */
    protected $cumulativeBenchmarkComparison;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCumulativeBenchmarkComparison(): ?CumulativeBenchmarkComparison
    {
        return $this->cumulativeBenchmarkComparison;
    }

    public function setCumulativeBenchmarkComparison(?CumulativeBenchmarkComparison $cumulativeBenchmarkComparison): self
    {
        $this->cumulativeBenchmarkComparison = $cumulativeBenchmarkComparison;

        return $this;
    }
}