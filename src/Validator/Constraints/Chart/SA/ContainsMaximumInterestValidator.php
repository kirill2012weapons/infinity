<?php

namespace App\Validator\Constraints\Chart\SA;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ContainsMaximumInterestValidator extends ConstraintValidator
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof ContainsMaximumInterest) {
            throw new UnexpectedTypeException($constraint, ContainsMaximumInterest::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!class_exists($constraint->getNameSpaceClass())) {
            throw new \Exception('Class not exist');
        }

        $results = $this->entityManager->createQueryBuilder()
            ->select('SUM(this.' . $constraint->getFieldName() . ')')
            ->from($constraint->getNameSpaceClass(), 'this')
            ->getQuery()
            ->getSingleScalarResult();

        if ((round($results, 7) + $value) >= 100) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ number }}', $results)
                ->addViolation();
        }
    }
}