<?php

namespace App\Validator\Constraints\Chart\SA;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsMaximumInterest extends Constraint
{
    protected $fieldName;

    protected $nameSpaceClass;

    public $message = 'Maximum can be only 100% in this column. Now - "{{ number }}"%, expected - 100%.';

    public function __construct($options)
    {
        parent::__construct();

        if($options['field_name']) {
            $this->fieldName = $options['field_name'];
        }
        if($options['name_space_class']) {
            $this->nameSpaceClass = $options['name_space_class'];
        }
    }

    public function getFieldName()
    {
        return $this->fieldName;
    }

    public function getNameSpaceClass()
    {
        return $this->nameSpaceClass;
    }

    public function validatedBy()
    {
        return \get_class($this).'Validator';
    }
}