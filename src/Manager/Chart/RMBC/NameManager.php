<?php

namespace App\Manager\Chart\RMBC;

use App\Entity\Charts\RMBC\Consolidated;
use App\Entity\Charts\RMBC\Efa;
use App\Entity\Charts\RMBC\Spxtr;
use App\Entity\Charts\RMBC\Vt;
use App\Model\Chart\RMBC\TypeNames;
use Doctrine\ORM\EntityManagerInterface;

class NameManager
{
    protected $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function getNames() : TypeNames
    {
        $typeNames = new TypeNames();

        $spxtrName = $this->entityManager->createQueryBuilder()
            ->select('s.name')
            ->from(Spxtr::class, 's')
            ->groupBy('s.name')
            ->getQuery()
            ->getResult();
        if (count($spxtrName) == 0) {
            return new TypeNames();
        }
        $typeNames->setSpxtrName($spxtrName[0]['name']);

        $consolidatedName = $this->entityManager->createQueryBuilder()
            ->select('c.name')
            ->from(Consolidated::class, 'c')
            ->groupBy('c.name')
            ->getQuery()
            ->getResult();
        if (count($consolidatedName) == 0) {
            return new TypeNames();
        }
        $typeNames->setConsolidatedName($consolidatedName[0]['name']);

        $efaName = $this->entityManager->createQueryBuilder()
            ->select('e.name')
            ->from(Efa::class, 'e')
            ->groupBy('e.name')
            ->getQuery()
            ->getResult();
        if (count($efaName) == 0) {
            return new TypeNames();
        }
        $typeNames->setEfaName($efaName[0]['name']);

        $vtName = $this->entityManager->createQueryBuilder()
            ->select('v.name')
            ->from(Vt::class, 'v')
            ->groupBy('v.name')
            ->getQuery()
            ->getResult();
        if (count($vtName) == 0) {
            return new TypeNames();
        }
        $typeNames->setVtName($vtName[0]['name']);

        return $typeNames;
    }

    public function updateName(TypeNames $typeNames)
    {
        $spxtrs       = $this->entityManager->getRepository(Spxtr::class)->findAll();
        $consolidates = $this->entityManager->getRepository(Consolidated::class)->findAll();
        $efaes        = $this->entityManager->getRepository(Efa::class)->findAll();
        $vts          = $this->entityManager->getRepository(Vt::class)->findAll();

        foreach ($spxtrs as $spxtr) {
            $spxtr->setName($typeNames->getSpxtrName());
        }

        foreach ($consolidates as $consolidate) {
            $consolidate->setName($typeNames->getConsolidatedName());
        }

        foreach ($efaes as $efa) {
            $efa->setName($typeNames->getEfaName());
        }

        foreach ($vts as $vt) {
            $vt->setName($typeNames->getVtName());
        }

        $this->entityManager->flush();
    }
}