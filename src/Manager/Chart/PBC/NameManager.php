<?php

namespace App\Manager\Chart\PBC;

use App\Entity\Charts\PBC\Consolidated;
use App\Entity\Charts\PBC\Efa;
use App\Entity\Charts\PBC\Spxtr;
use App\Entity\Charts\PBC\Vt;
use App\Model\Chart\PBC\TypeNames;
use Doctrine\ORM\EntityManagerInterface;

class NameManager
{
    protected $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function getNames() : TypeNames
    {
        $typeNames = new TypeNames();

        $spxtrName = $this->entityManager->createQueryBuilder()
            ->select('s.name')
            ->from(Spxtr::class, 's')
            ->groupBy('s.name')
            ->getQuery()
            ->getResult();
        if (count($spxtrName) == 0) {
            return new TypeNames();
        }
        $typeNames->setSpxtrName($spxtrName[0]['name']);

        $consolidatedName = $this->entityManager->createQueryBuilder()
            ->select('c.name')
            ->from(Consolidated::class, 'c')
            ->groupBy('c.name')
            ->getQuery()
            ->getResult();
        if (count($consolidatedName) == 0) {
            return new TypeNames();
        }
        $typeNames->setConsolidatedName($consolidatedName[0]['name']);

        $efaName = $this->entityManager->createQueryBuilder()
            ->select('e.name')
            ->from(Efa::class, 'e')
            ->groupBy('e.name')
            ->getQuery()
            ->getResult();
        if (count($efaName) == 0) {
            return new TypeNames();
        }
        $typeNames->setEfaName($efaName[0]['name']);

        $vtName = $this->entityManager->createQueryBuilder()
            ->select('v.name')
            ->from(Vt::class, 'v')
            ->groupBy('v.name')
            ->getQuery()
            ->getResult();
        if (count($vtName) == 0) {
            return new TypeNames();
        }
        $typeNames->setVtName($vtName[0]['name']);

        $spxtrColor = $this->entityManager->createQueryBuilder()
            ->select('s.color')
            ->from(Spxtr::class, 's')
            ->groupBy('s.color')
            ->getQuery()
            ->getResult();
        if (count($spxtrColor) == 0) {
            return new TypeNames();
        }
        $typeNames->setSpxtrColor($spxtrColor[0]['color']);

        $consolidatedColor = $this->entityManager->createQueryBuilder()
            ->select('c.color')
            ->from(Consolidated::class, 'c')
            ->groupBy('c.color')
            ->getQuery()
            ->getResult();
        if (count($consolidatedColor) == 0) {
            return new TypeNames();
        }
        $typeNames->setConsolidatedColor($consolidatedColor[0]['color']);

        $efaColor = $this->entityManager->createQueryBuilder()
            ->select('e.color')
            ->from(Efa::class, 'e')
            ->groupBy('e.color')
            ->getQuery()
            ->getResult();
        if (count($efaColor) == 0) {
            return new TypeNames();
        }
        $typeNames->setEfaColor($efaColor[0]['color']);

        $vtColor = $this->entityManager->createQueryBuilder()
            ->select('v.color')
            ->from(Vt::class, 'v')
            ->groupBy('v.color')
            ->getQuery()
            ->getResult();
        if (count($vtColor) == 0) {
            return new TypeNames();
        }
        $typeNames->setVtColor($vtColor[0]['color']);

        return $typeNames;
    }

    public function updateName(TypeNames $typeNames)
    {
        $spxtrs       = $this->entityManager->getRepository(Spxtr::class)->findAll();
        $consolidates = $this->entityManager->getRepository(Consolidated::class)->findAll();
        $efaes        = $this->entityManager->getRepository(Efa::class)->findAll();
        $vts          = $this->entityManager->getRepository(Vt::class)->findAll();

        foreach ($spxtrs as $spxtr) {
            $spxtr->setName($typeNames->getSpxtrName());
            $spxtr->setColor($typeNames->getSpxtrColor());
        }

        foreach ($consolidates as $consolidate) {
            $consolidate->setName($typeNames->getConsolidatedName());
            $consolidate->setColor($typeNames->getConsolidatedColor());
        }

        foreach ($efaes as $efa) {
            $efa->setName($typeNames->getEfaName());
            $efa->setColor($typeNames->getEfaColor());
        }

        foreach ($vts as $vt) {
            $vt->setName($typeNames->getVtName());
            $vt->setColor($typeNames->getVtColor());
        }

        $this->entityManager->flush();
    }
}