<?php


namespace App\DataFixtures\RMBC;

use App\Entity\Charts\RMBC\Consolidated;
use App\Entity\Charts\RMBC\Efa;
use App\Entity\Charts\RMBC\RiskMeasuresBenchmarkComparison;
use App\Entity\Charts\RMBC\Spxtr;
use App\Entity\Charts\RMBC\Vt;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RMBCFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $arrayFixtures = require __DIR__ . './../../../data_fixtures/RMBC/data.php';

        foreach ($arrayFixtures as $fixture) {

            $consolidated = (new Consolidated())
                ->setEndingVami($fixture['consolidated']['endingVami'])
                ->setMaxDrawdown($fixture['consolidated']['maxDrawdown'])
                ->setPeakToValley($fixture['consolidated']['peakToValley'])
                ->setRecovery($fixture['consolidated']['recovery'])
                ->setSharpeRatio($fixture['consolidated']['sharpeRatio'])
                ->setSortinoRatio($fixture['consolidated']['sortinoRatio'])
                ->setStandardDeviation($fixture['consolidated']['standardDeviation'])
                ->setDownsideDeviation($fixture['consolidated']['downsideDeviation'])
                ->setCorrelation($fixture['consolidated']['correlation'])
                ->setBeta($fixture['consolidated']['beta'])
                ->setAlpha($fixture['consolidated']['alpha'])
                ->setMeanReturn($fixture['consolidated']['meanReturn'])
                ->setPositivePeriods($fixture['consolidated']['positivePeriods'])
                ->setNegativePeriods($fixture['consolidated']['negativePeriods']);

            $spxtr        = (new Spxtr())
                ->setEndingVami($fixture['spxtr']['endingVami'])
                ->setMaxDrawdown($fixture['spxtr']['maxDrawdown'])
                ->setPeakToValley($fixture['spxtr']['peakToValley'])
                ->setRecovery($fixture['spxtr']['recovery'])
                ->setSharpeRatio($fixture['spxtr']['sharpeRatio'])
                ->setSortinoRatio($fixture['spxtr']['sortinoRatio'])
                ->setStandardDeviation($fixture['spxtr']['standardDeviation'])
                ->setDownsideDeviation($fixture['spxtr']['downsideDeviation'])
                ->setCorrelation($fixture['spxtr']['correlation'])
                ->setBeta($fixture['spxtr']['beta'])
                ->setAlpha($fixture['spxtr']['alpha'])
                ->setMeanReturn($fixture['spxtr']['meanReturn'])
                ->setPositivePeriods($fixture['spxtr']['positivePeriods'])
                ->setNegativePeriods($fixture['spxtr']['negativePeriods']);


            $efa          = (new Efa())
                ->setEndingVami($fixture['efa']['endingVami'])
                ->setMaxDrawdown($fixture['efa']['maxDrawdown'])
                ->setPeakToValley($fixture['efa']['peakToValley'])
                ->setRecovery($fixture['efa']['recovery'])
                ->setSharpeRatio($fixture['efa']['sharpeRatio'])
                ->setSortinoRatio($fixture['efa']['sortinoRatio'])
                ->setStandardDeviation($fixture['efa']['standardDeviation'])
                ->setDownsideDeviation($fixture['efa']['downsideDeviation'])
                ->setCorrelation($fixture['efa']['correlation'])
                ->setBeta($fixture['efa']['beta'])
                ->setAlpha($fixture['efa']['alpha'])
                ->setMeanReturn($fixture['efa']['meanReturn'])
                ->setPositivePeriods($fixture['efa']['positivePeriods'])
                ->setNegativePeriods($fixture['efa']['negativePeriods']);


            $vt           = (new Vt())
                ->setEndingVami($fixture['vt']['endingVami'])
                ->setMaxDrawdown($fixture['vt']['maxDrawdown'])
                ->setPeakToValley($fixture['vt']['peakToValley'])
                ->setRecovery($fixture['vt']['recovery'])
                ->setSharpeRatio($fixture['vt']['sharpeRatio'])
                ->setSortinoRatio($fixture['vt']['sortinoRatio'])
                ->setStandardDeviation($fixture['vt']['standardDeviation'])
                ->setDownsideDeviation($fixture['vt']['downsideDeviation'])
                ->setCorrelation($fixture['vt']['correlation'])
                ->setBeta($fixture['vt']['beta'])
                ->setAlpha($fixture['vt']['alpha'])
                ->setMeanReturn($fixture['vt']['meanReturn'])
                ->setPositivePeriods($fixture['vt']['positivePeriods'])
                ->setNegativePeriods($fixture['vt']['negativePeriods']);

            $riskMeasuresBenchmarkComparison = (new RiskMeasuresBenchmarkComparison())
                ->setSpxtr($spxtr)
                ->setConsolidated($consolidated)
                ->setVt($vt)
                ->setEfa($efa);

            $manager->persist($riskMeasuresBenchmarkComparison);
        }

        $manager->flush();
    }
}