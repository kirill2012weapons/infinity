<?php

namespace App\DataFixtures\PBAC;

use App\Entity\Charts\PBAC\PerformanceByAssetClass;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PbacFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $arrayFixtures = require __DIR__ . './../../../data_fixtures/PBAC/data.php';

        foreach ($arrayFixtures as $fixture) {

            $pbac = (new PerformanceByAssetClass())
                ->setDate(\DateTime::createFromFormat('Y m', $fixture['date']))
                ->setEquities($fixture['equities'])
                ->setRealEstate($fixture['realEstate'])
                ->setCash($fixture['cash']);

            $manager->persist($pbac);
        }

        $manager->flush();
    }
}