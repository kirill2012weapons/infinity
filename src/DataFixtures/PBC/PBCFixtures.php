<?php


namespace App\DataFixtures\PBC;

use App\Entity\Charts\PBC\Consolidated;
use App\Entity\Charts\PBC\CumulativeBenchmarkComparison;
use App\Entity\Charts\PBC\Efa;
use App\Entity\Charts\PBC\Spxtr;
use App\Entity\Charts\PBC\Vt;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PBCFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $arrayFixtures = require __DIR__ . './../../../data_fixtures/PBC/data.php';

        foreach ($arrayFixtures as $fixture) {
            $efa                           = (new Efa())
                ->setType(Efa::EFA_TYPE)
                ->setReturnValue($fixture['SPXTR']['return']);

            $consolidated                  = (new Consolidated())
                ->setType(Consolidated::CONSOLIDATED_TYPE)
                ->setReturnValue($fixture['CONSOLIDATED']['return']);

            $vt                            = (new Vt())
                ->setType(Vt::VT_TYPE)
                ->setReturnValue($fixture['VT']['return']);

            $spxtr                         = (new Spxtr())
                ->setType(Vt::SPXTR_TYPE)
                ->setReturnValue($fixture['SPXTR']['return']);

            $cumulativeBenchmarkComparison = (new CumulativeBenchmarkComparison())
                ->setDataType(CumulativeBenchmarkComparison::TYPE_CHART)
                ->setDate($fixture['date'])
                ->setEfa($efa)
                ->setConsolidated($consolidated)
                ->setVt($vt)
                ->setSpxtr($spxtr);

            $manager->persist($cumulativeBenchmarkComparison);
        }

        $manager->flush();
    }
}