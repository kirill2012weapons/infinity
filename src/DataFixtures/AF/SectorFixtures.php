<?php

namespace App\DataFixtures\AF;

use App\Entity\Charts\AF\Sector;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class SectorFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $arrayFixtures = require __DIR__ . './../../../data_fixtures/AF/sector.php';

        foreach ($arrayFixtures as $fixture) {
            $sector = (new Sector())
                ->setSlug($fixture);

            $this->addReference(self::name() . $fixture, $sector);

            $manager->persist($sector);
        }

        $manager->flush();
    }

    public static function name()
    {
        return self::class;
    }
}