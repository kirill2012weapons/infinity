<?php

namespace App\DataFixtures\AF;

use App\Entity\Charts\AF\AttributionEffect;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AfFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $arrayFixtures = require __DIR__ . './../../../data_fixtures/AF/data.php';

        foreach ($arrayFixtures as $fixture) {

            $af = (new AttributionEffect())
                ->setSector($this->getReference(SectorFixtures::name() . $fixture['sector']))
                ->setAllocation($fixture['allocation'])
                ->setSelection($fixture['selection'])
                ->setTotal($fixture['total']);

            $manager->persist($af);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            SectorFixtures::class,
        );
    }
}