<?php


namespace App\DataFixtures\PBS;

use App\Entity\Charts\PBS\PerformanceBySymbol;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PBSFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $arrayFixtures = require __DIR__ . './../../../data_fixtures/PBS/performance.php';

        foreach ($arrayFixtures as $fixture) {

            $pbs = (new PerformanceBySymbol())
                ->setSymbol($fixture['symbol'])
                ->setDescription($fixture['description'])
                ->setAvgWeight($fixture['avgWeight'])
                ->setReturnValue($fixture['returnValue'])
                ->setContribution($fixture['contribution'])
                ->setUnrealizedPL($fixture['unrealizedPL'])
                ->setRealizedPL($fixture['realizedPL'])
                ->setOpen($fixture['open'])
                ->setConsumerCyc($this->getReference($fixture['consumerCyc']))
                ->setConsumerNonCyc($this->getReference($fixture['consumerNonCyc']));

            $manager->persist($pbs);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            ConsumerCycFixtures::class,
            ConsumerNonCycFixtures::class,
        );
    }
}