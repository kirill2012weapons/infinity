<?php


namespace App\DataFixtures\PBS;

use App\Entity\Charts\PBS\ConsumerNonCyc;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ConsumerNonCycFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $arrayFixtures = require __DIR__ . './../../../data_fixtures/PBS/consumer_non.php';

        foreach ($arrayFixtures as $fixture) {

            $consumerNon = (new ConsumerNonCyc())
                ->setSlug($fixture);

            $this->addReference($fixture, $consumerNon);

            $manager->persist($consumerNon);
        }

        $manager->flush();
    }
}