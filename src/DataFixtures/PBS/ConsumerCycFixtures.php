<?php


namespace App\DataFixtures\PBS;

use App\Entity\Charts\PBS\ConsumerCyc;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ConsumerCycFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $arrayFixtures = require __DIR__ . './../../../data_fixtures/PBS/consumer.php';

        foreach ($arrayFixtures as $fixture) {

            $consumer = (new ConsumerCyc())
                ->setSlug($fixture);

            $this->addReference($fixture, $consumer);

            $manager->persist($consumer);
        }

        $manager->flush();
    }
}