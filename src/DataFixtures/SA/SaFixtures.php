<?php

namespace App\DataFixtures\SA;

use App\Entity\Charts\SA\SectorAllocation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class SaFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $arrayFixtures = require __DIR__ . './../../../data_fixtures/SA/data.php';

        foreach ($arrayFixtures as $fixture) {

            $af = (new SectorAllocation())
                ->setSector($this->getReference(SectorFixtures::name() . $fixture['sector']))
                ->setLongWeight($fixture['longWeight'])
                ->setLongParsedWeight($fixture['longParsedWeight'])
                ->setShortWeight($fixture['ShortWeight'])
                ->setShortParsedWeight($fixture['ShortParsedWeight']);

            $manager->persist($af);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            SectorFixtures::class,
        );
    }
}