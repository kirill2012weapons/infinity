<?php

namespace App\DataFixtures\CTR;

use App\Entity\Charts\CTR\ContributionToReturn;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CtrFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $arrayFixtures = require __DIR__ . './../../../data_fixtures/CTR/data.php';

        foreach ($arrayFixtures as $fixture) {

            $ctr = (new ContributionToReturn())
                ->setSector($this->getReference(SectorFixtures::name() . $fixture['sector']))
                ->setAccountContributionToReturn($fixture['accountContributionToReturn'])
                ->setBmContributionToReturn($fixture['bmContributionToReturn'])
                ->setContributionToReturnDifference($fixture['contributionToReturnDifference']);

            $manager->persist($ctr);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            SectorFixtures::class,
        );
    }
}