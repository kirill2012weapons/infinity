<?php

namespace App\Repository\Option\Performance;

use App\Entity\Option\Performance\PerformanceOption;
use Doctrine\ORM\EntityRepository;

class PerformanceOptionRepository extends EntityRepository
{
    /**
     * @return PerformanceOption|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getPerformance()
    {
        return $this->createQueryBuilder('po')
            ->getQuery()
            ->getOneOrNullResult();
    }
}