<?php

namespace App\Repository\Option\Performance;

use Doctrine\ORM\EntityRepository;

class PerformanceContentOptionRepository extends EntityRepository
{
    /**
     * @return int|mixed|string
     */
    public function getNotHideContents()
    {
        return $this->createQueryBuilder('pco')
            ->where('pco.display = 1')
            ->andWhere('pco.performance != \'null\'')
            ->getQuery()
            ->getResult();
    }
}