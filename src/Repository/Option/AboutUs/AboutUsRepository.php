<?php

namespace App\Repository\Option\AboutUs;

use Doctrine\ORM\EntityRepository;

class AboutUsRepository extends EntityRepository
{
    public function findLastOne()
    {
        return $this->createQueryBuilder('au')
            ->select('au')
            ->orderBy('au.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}