<?php

namespace App\Repository\Charts\CTR;

use App\Entity\Charts\CTR\ContributionToReturn;
use Doctrine\ORM\EntityRepository;

class ContributionToReturnRepository extends EntityRepository
{
    public function getForApiChart()
    {
        $result       = $this->findAll();

        if (empty($result)) return (object)[];

        $resultPretty = [];
        $labels       = [];
        $data         = [];
        $dataColors   = [];

        foreach ($result as $ctr) {
            /** @var $ctr ContributionToReturn */
            $labels[]     = $ctr->getSector()->getSlug();
            $data[]       = $ctr->getAccountContributionToReturn();
            $dataColors[] = 'gradientBarDown';
        }

        $resultPretty['labels']   = $labels;
        $resultPretty['datasets'] = [
            'data'            => $data,
            'backgroundColor' => $dataColors
        ];

        return $resultPretty;
    }
}