<?php

namespace App\Repository\Charts\PBC;

use App\Entity\Charts\PBC\CumulativeBenchmarkComparison;
use Doctrine\ORM\EntityRepository;

class CumulativeBenchmarkComparisonRepository extends EntityRepository
{
    public function getForApiChart()
    {
        $result       = $this->findAll();

        if (empty($result)) return (object)[];

        $resultPretty = [];

        $SPXTRValue        = [];
        $EFAValue          = [];
        $VTValue           = [];
        $CONSOLIDATEDValue = [];

        $dates             = [];

        foreach ($result as $cbc) {
            /** @var CumulativeBenchmarkComparison */
            $SPXTRValue['data'][]        = $cbc->getSpxtr()->getReturnValue();
            $SPXTRValue['label']         = strtoupper($cbc->getSpxtr()->getName());
            $SPXTRValue['color']         = $cbc->getSpxtr()->getColor();

            $EFAValue['data'][]          = $cbc->getEfa()->getReturnValue();
            $EFAValue['label']           = strtoupper($cbc->getEfa()->getName());
            $EFAValue['color']           = $cbc->getEfa()->getColor();

            $VTValue['data'][]           = $cbc->getVt()->getReturnValue();
            $VTValue['label']            = strtoupper($cbc->getVt()->getName());
            $VTValue['color']            = $cbc->getVt()->getColor();

            $CONSOLIDATEDValue['data'][] = $cbc->getConsolidated()->getReturnValue();
            $CONSOLIDATEDValue['label']  = strtoupper($cbc->getConsolidated()->getName());
            $CONSOLIDATEDValue['color']  = $cbc->getConsolidated()->getColor();

            $dates[] = $cbc->getDate();

        }

        $resultPretty['labels'] = $dates;
        if (!empty($SPXTRValue['label'])) {
            $resultPretty['datasets'][] = [
                'label'           => $SPXTRValue['label'],
                'data'            => $SPXTRValue['data'],
                'backgroundColor' => $SPXTRValue['color'],
            ];
        }
        if (!empty($EFAValue['label'])) {
            $resultPretty['datasets'][] = [
                'label'           => $EFAValue['label'],
                'data'            => $EFAValue['data'],
                'backgroundColor' => $EFAValue['color'],
            ];
        }
        if (!empty($VTValue['label'])) {
            $resultPretty['datasets'][] = [
                'label'           => $VTValue['label'],
                'data'            => $VTValue['data'],
                'backgroundColor' => $VTValue['color'],
            ];
        }
        if (!empty($CONSOLIDATEDValue['label'])) {
            $resultPretty['datasets'][] = [
                'label'           => $CONSOLIDATEDValue['label'],
                'data'            => $CONSOLIDATEDValue['data'],
                'backgroundColor' => $CONSOLIDATEDValue['color'],
            ];
        }

        return $resultPretty;
    }
}