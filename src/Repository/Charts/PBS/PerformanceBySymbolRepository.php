<?php

namespace App\Repository\Charts\PBS;

use App\Entity\Charts\PBS\PerformanceBySymbol;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;

class PerformanceBySymbolRepository extends EntityRepository
{
    public const VALUE_MAX = 150;
    public const VALUE_MIN = 65;

    public function getForApiChart()
    {
        $result       = $this->findAll();
        $resultPretty = [];

        if (empty($result)) return (object)[];

        $minValue = abs($result[0]->getReturnValue());
        $maxValue = abs($result[0]->getReturnValue());

        foreach ($result as $pbs) {
            $absValue = abs($pbs->getReturnValue());

            if ($minValue > $absValue) $minValue = $absValue;
            if ($maxValue < $absValue) $maxValue = $absValue;
        }

        $RRange   = (self::VALUE_MAX - self::VALUE_MIN);
        $ValRange = $maxValue - $minValue;

        $scale    = $ValRange / $RRange;

        foreach ($result as $pbs) {
            /** @var $pbs PerformanceBySymbol */

            $cR = ($maxValue - abs($pbs->getReturnValue())) / $scale;

            $resultPretty[] = [
                'name'   => $pbs->getSymbol(),
                'value'  => self::VALUE_MAX - $cR,
                'test'   => $pbs->getSymbol() . ':' . $pbs->getReturnValue(),
                'color'  => $pbs->getReturnValue() > 0 ? 'gradientCircleBlue' : 'gradientCircleOrange',
            ];
        }

        return $resultPretty;
    }

    public function getForApiChartContributorsTop($count)
    {
        $result = $this->createQueryBuilder('pbs')
            ->select('pbs')
            ->where('pbs.open =\'' . PerformanceBySymbol::STATE_OPEN . '\'')
            ->orderBy('pbs.avgWeight', 'DESC')
            ->setMaxResults($count)
            ->getQuery()
            ->getResult();

        $resultPretty = [];

        if (empty($result)) return (object)[];

        $labels = [];
        $data = [];

        foreach ($result as $pbs) {
            /** @var $pbs PerformanceBySymbol */

            $labels[] = $pbs->getSymbol();
            $data[] = $pbs->getAvgWeight();
        }

        $resultPretty['labels'] = $labels;
        $resultPretty['datasets'] = [
            'data' => $data,
            'backgroundColor' => 'secondChartBlue'
        ];

        return $resultPretty;
    }

    public function getForApiChartContributorsBottom($count)
    {
        $result = $this->createQueryBuilder('pbs')
            ->select('pbs')
            ->where('pbs.open =\'' . PerformanceBySymbol::STATE_CLOSED . '\'')
            ->orderBy('pbs.avgWeight', 'ASC')
            ->setMaxResults($count)
            ->getQuery()
            ->getResult();

        $resultPretty = [];

        if (empty($result)) return (object)[];

        $labels = [];
        $data = [];

        foreach ($result as $pbs) {
            /** @var $pbs PerformanceBySymbol */

            $labels[] = $pbs->getSymbol();
            $data[] = $pbs->getAvgWeight();
        }

        $resultPretty['labels'] = $labels;
        $resultPretty['datasets'] = [
            'data' => $data,
            'backgroundColor' => 'secondChartBlue'
        ];

        return $resultPretty;
    }


}