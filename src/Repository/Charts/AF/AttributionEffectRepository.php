<?php

namespace App\Repository\Charts\AF;

use App\Entity\Charts\AF\AttributionEffect;
use Doctrine\ORM\EntityRepository;

class AttributionEffectRepository extends EntityRepository
{
    public function getForApiChart()
    {
        $result       = $this->findAll();

        if (empty($result)) return (object)[];

        $resultPretty = [];

        $labels = [];
        $data   = [];
        $colors = [];

        foreach ($result as $af) {
            /** @var $af AttributionEffect */
            $labels[] = $af->getSector()->getSlug();
            $data[]   = $af->getAllocation();
            $colors[] = $af->getSector()->getColor();
        }

        $resultPretty['labels']   = $labels;
        $resultPretty['colors']   = $colors;
        $resultPretty['datasets'] = [
            'data'            => $data,
        ];

        return $resultPretty;
    }
}