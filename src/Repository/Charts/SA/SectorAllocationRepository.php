<?php

namespace App\Repository\Charts\SA;

use App\Entity\Charts\SA\SectorAllocation;
use Doctrine\ORM\EntityRepository;

class SectorAllocationRepository extends EntityRepository
{
    public function getForApiChart()
    {
        $result       = $this->findAll();

        if (empty($result)) return (object)[];

        $resultPretty = [];

        $labels = [];
        $data   = [];
        $colors = [];

        foreach ($result as $sa) {
            /** @var $sa SectorAllocation */
            $labels[] = $sa->getSector()->getSlug();
            $data[]   = $sa->getLongWeight();
            $colors[] = $sa->getSector()->getColor();
        }

        $resultPretty['labels']   = $labels;
        $resultPretty['colors']   = $colors;
        $resultPretty['datasets'] = [
            'data' => $data,
        ];

        return $resultPretty;
    }
}