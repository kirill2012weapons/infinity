<?php

namespace App\Repository\Charts\PBAC;

use App\Entity\Charts\PBAC\PerformanceByAssetClass;
use Doctrine\ORM\EntityRepository;

class PerformanceByAssetClassRepository extends EntityRepository
{
    public function getForApiChartDates()
    {
        $result       = $this->findAll();

        if (empty($result)) return (object)[];

        $resultPretty = [];

        $labels = [];
        $data   = [];

        $equities   = [];
        $realEstate = [];
        $cash       = [];
        foreach ($result as $pbac) {
            /** @var $pbac PerformanceByAssetClass */
            $labels[]     = $pbac->getDate()->format('Y m');
            $equities[]   = $pbac->getEquities();
            $realEstate[] = $pbac->getRealEstate();
            $cash[]       = $pbac->getCash();
        }

        $resultPretty['labels'] = $labels;
        $resultPretty['datasets'][] = [
            'label' => 'Equities',
            'data'  => $equities,
            'backgroundColor' => []
        ];
        $resultPretty['datasets'][] = [
            'label' => 'Real Estate',
            'data'  => $realEstate,
            'backgroundColor' => []
        ];
        $resultPretty['datasets'][] = [
            'label' => 'Cash',
            'data'  => $cash,
            'backgroundColor' => []
        ];

        return $resultPretty;
    }
}