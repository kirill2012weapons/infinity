<?php

namespace App\Repository\Auth;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function findByRole($role)
    {
        $qb = $this->createQueryBuilder('u')
            ->select('u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"'.$role.'"%');

        return $qb->getQuery()->getResult();
    }

    public function isExistUserName($userName)
    {
        return count($this->findBy(['username' =>$userName ])) >= 1 ? true : false;
    }

    public function isExistEmail($email)
    {
        return count($this->findBy(['email' =>$email ])) >= 1 ? true : false;
    }
}