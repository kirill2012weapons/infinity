<?php

namespace App\Twig;

use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\TwigTest;

class AppExtension extends AbstractExtension
{
    /**
     * @var RequestStack
     */
    protected $requestStack;

    public function __construct(RequestStack $requestStack) {

        $this->requestStack = $requestStack;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('dash', [$this, 'dashIfEmpty']),
        ];
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('menu_state', [$this, 'menuState']),
        ];
    }

    public function getTests()
    {
        return array(
            new TwigTest('instanceof', [$this, 'isInstanceOf'])
        );
    }

    public function dashIfEmpty($value)
    {
        if (empty($value) || ($value == null)) return '-';

        return $value;
    }

    public function menuState($routeName, $array = [])
    {
        $cRName = $this->requestStack->getCurrentRequest()->get('_route');

        if (empty($array)) {
            if (strpos($cRName, $routeName) === false) return '';

            return ' active';
        } else {
            foreach ($array as $routeNameArray) {
                if (strpos($cRName, $routeNameArray) === false) continue;

                return ' active';
            }

            return '';

        }
    }

    public function isInstanceOf($object, $class)
    {
        return  $object instanceof $class;
    }
}