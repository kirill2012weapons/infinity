$(document).ready(function () {

    $('.nav-icon').click(function () {
        $(this).toggleClass('open');
        $('.menu-drop').toggleClass('open');
    });

    $('.header .menu-drop .header-list a').click(function() {
        $('.menu-drop').removeClass('open');
        $('.nav-icon').removeClass('open');
    });

    //charts settings
    function hexToRgb(hex) {
        if (!hex) return {r:0,g:0,b:0};

        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function(m, r, g, b) {
            return r + r + g + g + b + b;
        });

        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : {r:0,g:0,b:0};
    }
    Chart.defaults.global.defaultFontColor = '#fff';

    if ($('#myChart').length > 0) {
        // var ctx = document.getElementById('myChart').getContext('2d');
        // var gradientBlue = ctx.createLinearGradient(0, 0, 0, 400);
        // gradientBlue.addColorStop(0, 'rgb(4, 227, 230)');
        // gradientBlue.addColorStop(1, 'rgba(39, 153, 163, 0.3)');
        //
        // var gradientOrange = ctx.createLinearGradient(0, 0, 0, 400);
        // gradientOrange.addColorStop(0, 'rgb(230, 177, 4)');
        // gradientOrange.addColorStop(1, 'rgba(230, 177, 4, 0.3)');
        //
        // var gradientGreen = ctx.createLinearGradient(0, 0, 0, 400);
        // gradientGreen.addColorStop(0, 'rgb(4, 230, 88)');
        // gradientGreen.addColorStop(1, 'rgba(4, 230, 88, 0.3)');
        //
        // var gradientRed = ctx.createLinearGradient(0, 0, 0, 400);
        // gradientRed.addColorStop(0, 'rgb(230, 4, 4)');
        // gradientRed.addColorStop(1, 'rgba(230, 4, 4, 0.3)');

        //get data firstChart
        let dataChartFirst = {};

        var ctx = document.getElementById('myChart').getContext('2d');
        $.ajax({
            dataType: "html",
            url: "/api/chart/cbc",
            method: 'GET',
            success: function (data) {
                dataChartFirst = JSON.parse(data);


                dataChartFirst.datasets.forEach(function (item) {
                    var gradient = ctx.createLinearGradient(0, 0, 0, 400);
                    gradient.addColorStop(0, 'rgb(' + hexToRgb(item.backgroundColor).r + ', ' + hexToRgb(item.backgroundColor).g + ', ' + hexToRgb(item.backgroundColor).b + ')');
                    gradient.addColorStop(1, 'rgba(' + hexToRgb(item.backgroundColor).r + ', ' + hexToRgb(item.backgroundColor).g + ', ' + hexToRgb(item.backgroundColor).b + ', 0.3)');
                    item.backgroundColor = gradient;
                });
                var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: dataChartFirst.labels,
                        datasets: dataChartFirst.datasets,
                    },
                    options: {
                        responsive: true,
                        tooltips: {
                            mode: 'index',
                        },
                        plugins: {
                            datalabels: {
                                display: false
                            }
                        },
                        hover: {
                            mode: 'index'
                        },
                        legend: {
                            display: true,
                            labels: {
                                position: 'right'
                            }
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    callback: function(value) {
                                        return value + ".00%"
                                    }
                                }
                            }]
                        },
                    }
                });
            }
        });
    }


    if ($('#myChart2').length > 0) {
        //chart 2/4
        var ctx2 = document.getElementById('myChart2').getContext('2d');

        var secondChartBlue = ctx2.createLinearGradient(285, 0, 0, 0);
        secondChartBlue.addColorStop(0, '#04E3E6');
        secondChartBlue.addColorStop(1, '#80808000');


        let dataChartSecond = {};

        $.ajax({
            dataType: "html",
            url: "/api/chart/pbac/dates",
            method: 'GET',
            success: function (data) {
                dataChartSecond = JSON.parse(data);

                let coloR = [];

                let dynamicColors = function() {
                    var r = Math.floor(Math.random() * 255);
                    var g = Math.floor(Math.random() * 255);
                    var b = Math.floor(Math.random() * 255);
                    return "rgb(" + r + "," + g + "," + b + ")";
                };

                for (let i in data) {
                    coloR.push(dynamicColors());
                }

                var myChart2 = new Chart(ctx2, {
                    type: 'bar',
                    responsive: true,
                    maintainAspectRatio: false,
                    data: {
                        labels: dataChartSecond.labels,
                        datasets: dataChartSecond.datasets,
                    },

                    options:{
                        plugins: {
                            datalabels: {
                                display: false
                            }
                        },
                        responsive: true,
                        legend: {
                            display: true,
                            position: 'bottom'
                        },
                        barBeginAtOrigin:true,
                        scales : {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true,
                                    // userCallback: function(label, index, labels) {
                                    //     if (Math.floor(label) === label) {
                                    //         return label;
                                    //     }
                                    // },
                                }
                            }],
                            xAxes : [ {
                                categoryPercentage: 1.0,
                                barPercentage: 0.3,
                                gridLines : {
                                    display : false
                                },
                                ticks: {

                                }
                            } ]
                        }
                    },


                });


                // dataChartSecond.datasets.forEach(function (item) {
                //
                //     item.backgroundColor.push('#fff','#fff','#fff','#fff','#fff','#fff','#fff');
                //
                // });
                dataChartSecond.datasets[0].backgroundColor.push('#04E3E6','#04E3E6','#04E3E6','#04E3E6','#04E3E6','#04E3E6','#04E3E6');
                dataChartSecond.datasets[1].backgroundColor.push('#F2AD45','#F2AD45','#F2AD45','#F2AD45','#F2AD45','#F2AD45','#F2AD45');
                dataChartSecond.datasets[2].backgroundColor.push('#17CF5F','#17CF5F','#17CF5F','#17CF5F','#17CF5F','#17CF5F','#17CF5F');
                console.log(dataChartSecond.datasets);
                myChart2.update();
            }

        });
    }


    if ($('#myChart6').length > 0) {
        //piecharts
        var ctx6 = document.getElementById('myChart6').getContext('2d');

        let dataChartSixth = {};

        $.ajax({
            dataType: "html",
            url: "/api/chart/sa",
            method: 'GET',
            success: function (data) {
                dataChartSixth = JSON.parse(data);
                let coloR = [];

                let dynamicColors = function() {
                    var r = Math.floor(Math.random() * 255);
                    var g = Math.floor(Math.random() * 255);
                    var b = Math.floor(Math.random() * 255);
                    return "rgb(" + r + "," + g + "," + b + ")";
                };

                for (let i in dataChartSixth.colors) {
                    coloR.push('rgb(' + hexToRgb(dataChartSixth.colors[i]).r + ', ' + hexToRgb(dataChartSixth.colors[i]).g + ', ' + hexToRgb(dataChartSixth.colors[i]).b + ')');
                }

                var myChart6 = new Chart(ctx6, {
                    type: 'pie',
                    data: {
                        labels: dataChartSixth.labels,
                        datasets: [{
                            data: dataChartSixth.datasets.data,
                            backgroundColor: coloR
                        }]
                    },
                    options:{
                        elements: {
                            arc: {
                                borderWidth: 0
                            }
                        },
                        responsive: true,
                        legend: {
                            display: true,
                            position: 'bottom'
                        },
                        plugins: {
                            datalabels: {
                                display: false,
                                // formatter: function (value) {
                                //     return Math.round(value) + '%';
                                // },
                                // anchor: 'end',
                                // align: 'start',
                                // font: {
                                //     size: 10,
                                // }
                            }
                        },
                        scales: {
                            yAxes: [{
                                display: false,
                            }],
                            xAxes : [ {
                                display: false,
                            } ]

                        },
                    }
                });
            }

        });
    }

    if ($('#myChart3').length > 0) {
        //chart 3
        var ctx3 = document.getElementById('myChart3').getContext('2d');

        var gradientBarUp = ctx3.createLinearGradient(0, 0, 0, 200);
        gradientBarUp.addColorStop(0, '#04E3E6');
        gradientBarUp.addColorStop(1, '#ECAA4600');

        var gradientBarDown = ctx3.createLinearGradient(0, 0, 0, 350);
        gradientBarDown.addColorStop(0, '#04E3E600');
        gradientBarDown.addColorStop(1, '#ECAA46');

        var chartColors = {
            up: gradientBarUp,
            down: gradientBarDown
        };
        //get data thirdChart
        let dataChartThird = {};

        $.ajax({
            dataType: "html",
            url: "/api/chart/ctr/by-sector",
            method: 'GET',
            success: function (data) {
                dataChartThird = JSON.parse(data);

                var myChart3 = new Chart(ctx3, {
                    type: 'bar',
                    responsive: true,
                    maintainAspectRatio: false,
                    data: {
                        labels: dataChartThird.labels,
                        datasets: [{
                            data: dataChartThird.datasets.data,
                            backgroundColor:[
                                chartColors.down,
                                chartColors.down,
                                chartColors.down,
                                chartColors.down,
                                chartColors.down,
                                chartColors.down,
                                chartColors.down,
                                chartColors.down,
                                chartColors.down,
                                chartColors.down,
                                chartColors.down,
                                chartColors.down,
                                chartColors.down
                            ],
                        }]
                    },

                    options:{
                        plugins: {
                            datalabels: {
                                formatter: function (value) {
                                    return Math.round(value) + '%';
                                },
                                align: 'center',
                                font: {
                                    size: 10,
                                }
                            }
                        },
                        responsive: true,
                        legend: {
                            display: false
                        },
                        barBeginAtOrigin:true,
                        scales : {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }],
                            xAxes : [ {
                                gridLines : {
                                    display : false
                                }
                            } ]
                        }
                    }
                });

                var colorChangeValue = 0; //set this to whatever is the deciding color change value
                var dataset = myChart3.data.datasets[0];


                for (var i = 0; i < dataset.data.length; i++) {
                    if (dataset.data[i] > colorChangeValue) {
                        dataset.backgroundColor[i] = chartColors.up;
                    }
                }
                myChart3.update();
            }

        });
    }


    if ($('#myChart5').length > 0) {
        //chart 5
        var ctx5 = document.getElementById('myChart5').getContext('2d');

        let dataChartFifth = {};

        $.ajax({
            dataType: "html",
            url: "/api/chart/af",
            method: 'GET',
            success: function (data) {
                dataChartFifth = JSON.parse(data);

                let coloR = [];

                let dynamicColors = function() {
                    var r = Math.floor(Math.random() * 255);
                    var g = Math.floor(Math.random() * 255);
                    var b = Math.floor(Math.random() * 255);
                    return "rgb(" + r + "," + g + "," + b + ")";
                };

                for (let i in dataChartFifth.colors) {
                    coloR.push('rgb(' + hexToRgb(dataChartFifth.colors[i]).r + ', ' + hexToRgb(dataChartFifth.colors[i]).g + ', ' + hexToRgb(dataChartFifth.colors[i]).b + ')');
                }

                var myChart5 = new Chart(ctx5, {
                    type: 'bar',
                    responsive: true,
                    maintainAspectRatio: false,
                    data: {
                        labels: dataChartFifth.labels,
                        datasets: [{
                            data: dataChartFifth.datasets.data,
                            backgroundColor: coloR
                        }],
                    },

                    options:{
                        plugins: {
                            datalabels: {
                                display: false
                            }
                        },
                        responsive: true,
                        legend: {
                            display: false,
                            position: 'bottom'
                        },
                        barBeginAtOrigin:true,
                        scales : {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true,
                                    // userCallback: function(label, index, labels) {
                                    //     if (Math.floor(label) === label) {
                                    //         return label;
                                    //     }
                                    // },
                                }
                            }],
                            xAxes : [ {
                                categoryPercentage: 1.0,
                                barPercentage: 0.3,
                                gridLines : {
                                    display : false
                                },
                                ticks: {

                                }
                            } ]
                        }
                    }
                });
            }

        });
    }



});

if ($('#chartdiv').length > 0) {
    am4core.ready(function () {
        let dataChartBulb = [];
        $.ajax({
            dataType: "html",
            url: "/api/chart/pbc",
            method: 'GET',
            success: function (data) {
                dataChartBulb = JSON.parse(data);

                //Themes begin
                am4core.useTheme(am4themes_animated);
                //Themes end
                var chart = am4core.create("chartdiv", am4plugins_forceDirected.ForceDirectedTree);
                var networkSeries = chart.series.push(new am4plugins_forceDirected.ForceDirectedSeries());

                let gradientCircleBlue = new am4core.LinearGradient();
                gradientCircleBlue.rotation = 90;
                gradientCircleBlue.addColor(am4core.color("#18D4D6"));
                gradientCircleBlue.addColor(am4core.color("rgba(24, 212, 214, 0.21)"));

                let gradientCircleOrange = new am4core.LinearGradient();
                gradientCircleOrange.rotation = 90;
                gradientCircleOrange.addColor(am4core.color("rgba(255, 118, 0, 0.4)"));
                gradientCircleOrange.addColor(am4core.color("#FF7600"));

                dataChartBulb.forEach(function (item) {

                    if(item.color === 'gradientCircleBlue'){
                        item.color = gradientCircleBlue
                    } else {
                        item.color = gradientCircleOrange
                    }

                });
                chart.data = dataChartBulb;
                networkSeries.dataFields.value = "value";
                networkSeries.dataFields.name = "name";
                networkSeries.dataFields.color = "color";
                networkSeries.nodes.template.tooltipText = "{test}";
                networkSeries.nodes.template.fillOpacity = 0.7;
                networkSeries.dataFields.id = "name";
                networkSeries.dataFields.linkWith = "link";
                networkSeries.nodes.template.label.text = "{test}";
                networkSeries.nodes.template.strokeWidth = 4;
                networkSeries.fontSize = 12;
            }

        });
    });
}