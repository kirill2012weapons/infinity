
PHP ---------- 7.1.33

MySQL ------ 5.6

===============================================================================

Run - `composer install`

Run - `npm i`

Run - `npm watch`

===============================================================================

MIGRATIONS:............... `php bin/console doc:mig:mig`

FIXTURES:.................... `php bin/console doc:fix:load`

CREATE USER ADMIN: `php bin/console fos:user:create admin apdmin@gmail.com admin --super-admin`