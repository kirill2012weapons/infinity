var Encore = require('@symfony/webpack-encore');

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .addEntry('app',       './assets/js/app.js')
    .addEntry('app_admin', './assets/js/app_admin.js')

    .copyFiles({
        from: './assets/admin',
        to: 'admin/[path][name].[ext]',
        pattern: /\.(png|jpg|jpeg|css|js|woff2)$/
    })

    .copyFiles({
        from: './assets/front',
        to: 'front/[path][name].[ext]',
        pattern: /\.(png|jpg|jpeg|css|js|woff2|eot|woff|ttf|json)$/
    })

    .enableSassLoader()

    .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })
;

module.exports = Encore.getWebpackConfig();
