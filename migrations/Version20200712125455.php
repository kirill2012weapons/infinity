<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200712125455 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE options_about_us (id INT AUTO_INCREMENT NOT NULL, white_title VARCHAR(255) NOT NULL, blue_title VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE options_about_us_part (id INT AUTO_INCREMENT NOT NULL, about_us_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, image VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, INDEX IDX_E1690C6A7CE2CF2D (about_us_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE options_about_us_part ADD CONSTRAINT FK_E1690C6A7CE2CF2D FOREIGN KEY (about_us_id) REFERENCES options_about_us (id)');
        $this->addSql('ALTER TABLE af_attribution_effect CHANGE allocation allocation FLOAT, CHANGE selection selection FLOAT, CHANGE total total FLOAT');
        $this->addSql('ALTER TABLE ctr_contribution_to_return CHANGE account_contribution_to_return account_contribution_to_return FLOAT, CHANGE bm_contribution_to_return bm_contribution_to_return FLOAT, CHANGE contribution_to_return_difference contribution_to_return_difference FLOAT');
        $this->addSql('ALTER TABLE pbac_performance_by_asset_class CHANGE equities equities FLOAT, CHANGE real_estate real_estate FLOAT, CHANGE cash cash FLOAT');
        $this->addSql('ALTER TABLE pbc_consolidated CHANGE return_value return_value FLOAT');
        $this->addSql('ALTER TABLE pbc_efa CHANGE return_value return_value FLOAT');
        $this->addSql('ALTER TABLE pbc_spxtr CHANGE return_value return_value FLOAT');
        $this->addSql('ALTER TABLE pbc_vt CHANGE return_value return_value FLOAT');
        $this->addSql('ALTER TABLE pbs_performance_by_symbol CHANGE avg_weight avg_weight FLOAT, CHANGE return_value return_value FLOAT, CHANGE contribution contribution FLOAT, CHANGE unrealized_pl unrealized_pl FLOAT, CHANGE realized_pl realized_pl FLOAT');
        $this->addSql('ALTER TABLE sa_sector_allocation CHANGE long_weight long_weight FLOAT, CHANGE long_parsed_weight long_parsed_weight FLOAT, CHANGE short_weight short_weight FLOAT, CHANGE short_parsed_weight short_parsed_weight FLOAT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE options_about_us_part DROP FOREIGN KEY FK_E1690C6A7CE2CF2D');
        $this->addSql('DROP TABLE options_about_us');
        $this->addSql('DROP TABLE options_about_us_part');
        $this->addSql('ALTER TABLE af_attribution_effect CHANGE allocation allocation DOUBLE PRECISION DEFAULT NULL, CHANGE selection selection DOUBLE PRECISION DEFAULT NULL, CHANGE total total DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE ctr_contribution_to_return CHANGE account_contribution_to_return account_contribution_to_return DOUBLE PRECISION DEFAULT NULL, CHANGE bm_contribution_to_return bm_contribution_to_return DOUBLE PRECISION DEFAULT NULL, CHANGE contribution_to_return_difference contribution_to_return_difference DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbac_performance_by_asset_class CHANGE equities equities DOUBLE PRECISION DEFAULT NULL, CHANGE real_estate real_estate DOUBLE PRECISION DEFAULT NULL, CHANGE cash cash DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbc_consolidated CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbc_efa CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbc_spxtr CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbc_vt CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbs_performance_by_symbol CHANGE avg_weight avg_weight DOUBLE PRECISION DEFAULT NULL, CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL, CHANGE contribution contribution DOUBLE PRECISION DEFAULT NULL, CHANGE unrealized_pl unrealized_pl DOUBLE PRECISION DEFAULT NULL, CHANGE realized_pl realized_pl DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE sa_sector_allocation CHANGE long_weight long_weight DOUBLE PRECISION DEFAULT NULL, CHANGE long_parsed_weight long_parsed_weight DOUBLE PRECISION DEFAULT NULL, CHANGE short_weight short_weight DOUBLE PRECISION DEFAULT NULL, CHANGE short_parsed_weight short_parsed_weight DOUBLE PRECISION DEFAULT NULL');
    }
}
