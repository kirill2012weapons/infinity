<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200703155339 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE pbac_performance_by_asset_class (id INT AUTO_INCREMENT NOT NULL, date DATE NOT NULL, equities FLOAT, real_estate FLOAT, cash FLOAT, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ctr_contribution_to_return (id INT AUTO_INCREMENT NOT NULL, sector_id INT NOT NULL, account_contribution_to_return FLOAT, bm_contribution_to_return FLOAT, contribution_to_return_difference FLOAT, INDEX IDX_197E9B81DE95C867 (sector_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ctr_sector (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE af_attribution_effect (id INT AUTO_INCREMENT NOT NULL, sector_id INT NOT NULL, allocation FLOAT, selection FLOAT, total FLOAT, INDEX IDX_B6EDB316DE95C867 (sector_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE af_sector (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ctr_contribution_to_return ADD CONSTRAINT FK_197E9B81DE95C867 FOREIGN KEY (sector_id) REFERENCES ctr_sector (id)');
        $this->addSql('ALTER TABLE af_attribution_effect ADD CONSTRAINT FK_B6EDB316DE95C867 FOREIGN KEY (sector_id) REFERENCES af_sector (id)');
        $this->addSql('ALTER TABLE pbc_efa CHANGE return_value return_value FLOAT');
        $this->addSql('ALTER TABLE pbc_consolidated CHANGE return_value return_value FLOAT');
        $this->addSql('ALTER TABLE pbc_vt CHANGE return_value return_value FLOAT');
        $this->addSql('ALTER TABLE pbc_spxtr CHANGE return_value return_value FLOAT');
        $this->addSql('ALTER TABLE pbs_performance_by_symbol CHANGE avg_weight avg_weight FLOAT, CHANGE return_value return_value FLOAT, CHANGE contribution contribution FLOAT, CHANGE unrealized_pl unrealized_pl FLOAT, CHANGE realized_pl realized_pl FLOAT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ctr_contribution_to_return DROP FOREIGN KEY FK_197E9B81DE95C867');
        $this->addSql('ALTER TABLE af_attribution_effect DROP FOREIGN KEY FK_B6EDB316DE95C867');
        $this->addSql('DROP TABLE pbac_performance_by_asset_class');
        $this->addSql('DROP TABLE ctr_contribution_to_return');
        $this->addSql('DROP TABLE ctr_sector');
        $this->addSql('DROP TABLE af_attribution_effect');
        $this->addSql('DROP TABLE af_sector');
        $this->addSql('ALTER TABLE pbc_consolidated CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbc_efa CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbc_spxtr CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbc_vt CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbs_performance_by_symbol CHANGE avg_weight avg_weight DOUBLE PRECISION DEFAULT NULL, CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL, CHANGE contribution contribution DOUBLE PRECISION DEFAULT NULL, CHANGE unrealized_pl unrealized_pl DOUBLE PRECISION DEFAULT NULL, CHANGE realized_pl realized_pl DOUBLE PRECISION DEFAULT NULL');
    }
}
