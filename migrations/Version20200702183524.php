<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200702183524 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE pbs_consumer_cyc (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pbs_consumer_non_cyc (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pbs_performance_by_symbol (id INT AUTO_INCREMENT NOT NULL, consumer_cyc_id INT NOT NULL, consumer_non_cyc_id INT NOT NULL, symbol VARCHAR(50) NOT NULL, description LONGTEXT DEFAULT NULL, avg_weight FLOAT, return_value FLOAT, contribution FLOAT, unrealized_pl FLOAT, realized_pl FLOAT, open VARCHAR(255) NOT NULL, INDEX IDX_74E46FEBB55993E4 (consumer_cyc_id), INDEX IDX_74E46FEB6F273FC4 (consumer_non_cyc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pbs_performance_by_symbol ADD CONSTRAINT FK_74E46FEBB55993E4 FOREIGN KEY (consumer_cyc_id) REFERENCES pbs_consumer_cyc (id)');
        $this->addSql('ALTER TABLE pbs_performance_by_symbol ADD CONSTRAINT FK_74E46FEB6F273FC4 FOREIGN KEY (consumer_non_cyc_id) REFERENCES pbs_consumer_non_cyc (id)');
        $this->addSql('ALTER TABLE pbc_consolidated CHANGE return_value return_value FLOAT');
        $this->addSql('ALTER TABLE pbc_efa CHANGE return_value return_value FLOAT');
        $this->addSql('ALTER TABLE pbc_spxtr CHANGE return_value return_value FLOAT');
        $this->addSql('ALTER TABLE pbc_vt CHANGE return_value return_value FLOAT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pbs_performance_by_symbol DROP FOREIGN KEY FK_74E46FEBB55993E4');
        $this->addSql('ALTER TABLE pbs_performance_by_symbol DROP FOREIGN KEY FK_74E46FEB6F273FC4');
        $this->addSql('DROP TABLE pbs_consumer_cyc');
        $this->addSql('DROP TABLE pbs_consumer_non_cyc');
        $this->addSql('DROP TABLE pbs_performance_by_symbol');
        $this->addSql('ALTER TABLE pbc_consolidated CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbc_efa CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbc_spxtr CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbc_vt CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
    }
}
