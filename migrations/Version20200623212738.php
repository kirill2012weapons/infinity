<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200623212738 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE rmbc_consolidated (id INT AUTO_INCREMENT NOT NULL, rmbc_id INT DEFAULT NULL, type VARCHAR(50) NOT NULL, ending_vami FLOAT, max_drawdown FLOAT, peak_to_valley VARCHAR(255) NOT NULL, recovery VARCHAR(255) NOT NULL, sharpe_ratio FLOAT, sortino_ratio FLOAT, standard_deviation FLOAT, downside_deviation FLOAT, correlation FLOAT, beta FLOAT, alpha FLOAT, mean_return FLOAT, positive_periods VARCHAR(255) NOT NULL, negative_periods VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_63D1BA16190621A (rmbc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rmbc_efa (id INT AUTO_INCREMENT NOT NULL, rmbc_id INT DEFAULT NULL, type VARCHAR(50) NOT NULL, ending_vami FLOAT, max_drawdown FLOAT, peak_to_valley VARCHAR(255) NOT NULL, recovery VARCHAR(255) NOT NULL, sharpe_ratio FLOAT, sortino_ratio FLOAT, standard_deviation FLOAT, downside_deviation FLOAT, correlation FLOAT, beta FLOAT, alpha FLOAT, mean_return FLOAT, positive_periods VARCHAR(255) NOT NULL, negative_periods VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_2EF84CBD190621A (rmbc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rmbc_risk_measures_benchmark_comparison (id INT AUTO_INCREMENT NOT NULL, data_type VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rmbc_spxtr (id INT AUTO_INCREMENT NOT NULL, rmbc_id INT DEFAULT NULL, type VARCHAR(50) NOT NULL, ending_vami FLOAT, max_drawdown FLOAT, peak_to_valley VARCHAR(255) NOT NULL, recovery VARCHAR(255) NOT NULL, sharpe_ratio FLOAT, sortino_ratio FLOAT, standard_deviation FLOAT, downside_deviation FLOAT, correlation FLOAT, beta FLOAT, alpha FLOAT, mean_return FLOAT, positive_periods VARCHAR(255) NOT NULL, negative_periods VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_13A5BF35190621A (rmbc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rmbc_vt (id INT AUTO_INCREMENT NOT NULL, rmbc_id INT DEFAULT NULL, type VARCHAR(50) NOT NULL, ending_vami FLOAT, max_drawdown FLOAT, peak_to_valley VARCHAR(255) NOT NULL, recovery VARCHAR(255) NOT NULL, sharpe_ratio FLOAT, sortino_ratio FLOAT, standard_deviation FLOAT, downside_deviation FLOAT, correlation FLOAT, beta FLOAT, alpha FLOAT, mean_return FLOAT, positive_periods VARCHAR(255) NOT NULL, negative_periods VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_D17D7CE0190621A (rmbc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE rmbc_consolidated ADD CONSTRAINT FK_63D1BA16190621A FOREIGN KEY (rmbc_id) REFERENCES rmbc_risk_measures_benchmark_comparison (id)');
        $this->addSql('ALTER TABLE rmbc_efa ADD CONSTRAINT FK_2EF84CBD190621A FOREIGN KEY (rmbc_id) REFERENCES rmbc_risk_measures_benchmark_comparison (id)');
        $this->addSql('ALTER TABLE rmbc_spxtr ADD CONSTRAINT FK_13A5BF35190621A FOREIGN KEY (rmbc_id) REFERENCES rmbc_risk_measures_benchmark_comparison (id)');
        $this->addSql('ALTER TABLE rmbc_vt ADD CONSTRAINT FK_D17D7CE0190621A FOREIGN KEY (rmbc_id) REFERENCES rmbc_risk_measures_benchmark_comparison (id)');
        $this->addSql('ALTER TABLE pbc_consolidated CHANGE return_value return_value FLOAT');
        $this->addSql('ALTER TABLE pbc_efa CHANGE return_value return_value FLOAT');
        $this->addSql('ALTER TABLE pbc_spxtr CHANGE return_value return_value FLOAT');
        $this->addSql('ALTER TABLE pbc_vt CHANGE return_value return_value FLOAT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rmbc_consolidated DROP FOREIGN KEY FK_63D1BA16190621A');
        $this->addSql('ALTER TABLE rmbc_efa DROP FOREIGN KEY FK_2EF84CBD190621A');
        $this->addSql('ALTER TABLE rmbc_spxtr DROP FOREIGN KEY FK_13A5BF35190621A');
        $this->addSql('ALTER TABLE rmbc_vt DROP FOREIGN KEY FK_D17D7CE0190621A');
        $this->addSql('DROP TABLE rmbc_consolidated');
        $this->addSql('DROP TABLE rmbc_efa');
        $this->addSql('DROP TABLE rmbc_risk_measures_benchmark_comparison');
        $this->addSql('DROP TABLE rmbc_spxtr');
        $this->addSql('DROP TABLE rmbc_vt');
        $this->addSql('ALTER TABLE pbc_consolidated CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbc_efa CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbc_spxtr CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbc_vt CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
    }
}
