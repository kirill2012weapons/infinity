<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200622194419 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE consolidated ADD return_value FLOAT, DROP `return`');
        $this->addSql('ALTER TABLE efa ADD return_value FLOAT, DROP `return`');
        $this->addSql('ALTER TABLE spxtr ADD return_value FLOAT, DROP `return`');
        $this->addSql('ALTER TABLE vt ADD return_value FLOAT, DROP `return`');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE consolidated ADD `return` DOUBLE PRECISION DEFAULT NULL, DROP return_value');
        $this->addSql('ALTER TABLE efa ADD `return` DOUBLE PRECISION DEFAULT NULL, DROP return_value');
        $this->addSql('ALTER TABLE spxtr ADD `return` DOUBLE PRECISION DEFAULT NULL, DROP return_value');
        $this->addSql('ALTER TABLE vt ADD `return` DOUBLE PRECISION DEFAULT NULL, DROP return_value');
    }
}
