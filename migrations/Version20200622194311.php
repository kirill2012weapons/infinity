<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200622194311 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE consolidated (id INT AUTO_INCREMENT NOT NULL, cbc_id INT DEFAULT NULL, type VARCHAR(50) NOT NULL, `return` FLOAT, UNIQUE INDEX UNIQ_6596A06A6CC3BCB5 (cbc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cumulative_benchmark_comparison (id INT AUTO_INCREMENT NOT NULL, data_type VARCHAR(50) NOT NULL, date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE efa (id INT AUTO_INCREMENT NOT NULL, cbc_id INT DEFAULT NULL, type VARCHAR(50) NOT NULL, `return` FLOAT, UNIQUE INDEX UNIQ_B84F4D366CC3BCB5 (cbc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE spxtr (id INT AUTO_INCREMENT NOT NULL, cbc_id INT DEFAULT NULL, type VARCHAR(50) NOT NULL, `return` FLOAT, UNIQUE INDEX UNIQ_BCD558946CC3BCB5 (cbc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vt (id INT AUTO_INCREMENT NOT NULL, cbc_id INT DEFAULT NULL, type VARCHAR(50) NOT NULL, `return` FLOAT, UNIQUE INDEX UNIQ_6FD479AA6CC3BCB5 (cbc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE consolidated ADD CONSTRAINT FK_6596A06A6CC3BCB5 FOREIGN KEY (cbc_id) REFERENCES cumulative_benchmark_comparison (id)');
        $this->addSql('ALTER TABLE efa ADD CONSTRAINT FK_B84F4D366CC3BCB5 FOREIGN KEY (cbc_id) REFERENCES cumulative_benchmark_comparison (id)');
        $this->addSql('ALTER TABLE spxtr ADD CONSTRAINT FK_BCD558946CC3BCB5 FOREIGN KEY (cbc_id) REFERENCES cumulative_benchmark_comparison (id)');
        $this->addSql('ALTER TABLE vt ADD CONSTRAINT FK_6FD479AA6CC3BCB5 FOREIGN KEY (cbc_id) REFERENCES cumulative_benchmark_comparison (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE consolidated DROP FOREIGN KEY FK_6596A06A6CC3BCB5');
        $this->addSql('ALTER TABLE efa DROP FOREIGN KEY FK_B84F4D366CC3BCB5');
        $this->addSql('ALTER TABLE spxtr DROP FOREIGN KEY FK_BCD558946CC3BCB5');
        $this->addSql('ALTER TABLE vt DROP FOREIGN KEY FK_6FD479AA6CC3BCB5');
        $this->addSql('DROP TABLE consolidated');
        $this->addSql('DROP TABLE cumulative_benchmark_comparison');
        $this->addSql('DROP TABLE efa');
        $this->addSql('DROP TABLE spxtr');
        $this->addSql('DROP TABLE vt');
    }
}
