<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200623202519 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE consolidated DROP FOREIGN KEY FK_6596A06A6CC3BCB5');
        $this->addSql('ALTER TABLE efa DROP FOREIGN KEY FK_B84F4D366CC3BCB5');
        $this->addSql('ALTER TABLE spxtr DROP FOREIGN KEY FK_BCD558946CC3BCB5');
        $this->addSql('ALTER TABLE vt DROP FOREIGN KEY FK_6FD479AA6CC3BCB5');
        $this->addSql('CREATE TABLE pbc_cumulative_benchmark_comparison (id INT AUTO_INCREMENT NOT NULL, data_type VARCHAR(50) NOT NULL, date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pbc_consolidated (id INT AUTO_INCREMENT NOT NULL, cbc_id INT DEFAULT NULL, type VARCHAR(50) NOT NULL, return_value FLOAT, UNIQUE INDEX UNIQ_1960228D6CC3BCB5 (cbc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pbc_efa (id INT AUTO_INCREMENT NOT NULL, cbc_id INT DEFAULT NULL, type VARCHAR(50) NOT NULL, return_value FLOAT, UNIQUE INDEX UNIQ_3ECA27496CC3BCB5 (cbc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pbc_spxtr (id INT AUTO_INCREMENT NOT NULL, cbc_id INT DEFAULT NULL, type VARCHAR(50) NOT NULL, return_value FLOAT, UNIQUE INDEX UNIQ_B915336E6CC3BCB5 (cbc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pbc_vt (id INT AUTO_INCREMENT NOT NULL, cbc_id INT DEFAULT NULL, type VARCHAR(50) NOT NULL, return_value FLOAT, UNIQUE INDEX UNIQ_396CD27A6CC3BCB5 (cbc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pbc_consolidated ADD CONSTRAINT FK_1960228D6CC3BCB5 FOREIGN KEY (cbc_id) REFERENCES pbc_cumulative_benchmark_comparison (id)');
        $this->addSql('ALTER TABLE pbc_efa ADD CONSTRAINT FK_3ECA27496CC3BCB5 FOREIGN KEY (cbc_id) REFERENCES pbc_cumulative_benchmark_comparison (id)');
        $this->addSql('ALTER TABLE pbc_spxtr ADD CONSTRAINT FK_B915336E6CC3BCB5 FOREIGN KEY (cbc_id) REFERENCES pbc_cumulative_benchmark_comparison (id)');
        $this->addSql('ALTER TABLE pbc_vt ADD CONSTRAINT FK_396CD27A6CC3BCB5 FOREIGN KEY (cbc_id) REFERENCES pbc_cumulative_benchmark_comparison (id)');
        $this->addSql('DROP TABLE consolidated');
        $this->addSql('DROP TABLE cumulative_benchmark_comparison');
        $this->addSql('DROP TABLE efa');
        $this->addSql('DROP TABLE spxtr');
        $this->addSql('DROP TABLE vt');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pbc_consolidated DROP FOREIGN KEY FK_1960228D6CC3BCB5');
        $this->addSql('ALTER TABLE pbc_efa DROP FOREIGN KEY FK_3ECA27496CC3BCB5');
        $this->addSql('ALTER TABLE pbc_spxtr DROP FOREIGN KEY FK_B915336E6CC3BCB5');
        $this->addSql('ALTER TABLE pbc_vt DROP FOREIGN KEY FK_396CD27A6CC3BCB5');
        $this->addSql('CREATE TABLE consolidated (id INT AUTO_INCREMENT NOT NULL, cbc_id INT DEFAULT NULL, type VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, return_value DOUBLE PRECISION DEFAULT NULL, UNIQUE INDEX UNIQ_6596A06A6CC3BCB5 (cbc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE cumulative_benchmark_comparison (id INT AUTO_INCREMENT NOT NULL, data_type VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE efa (id INT AUTO_INCREMENT NOT NULL, cbc_id INT DEFAULT NULL, type VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, return_value DOUBLE PRECISION DEFAULT NULL, UNIQUE INDEX UNIQ_B84F4D366CC3BCB5 (cbc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE spxtr (id INT AUTO_INCREMENT NOT NULL, cbc_id INT DEFAULT NULL, type VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, return_value DOUBLE PRECISION DEFAULT NULL, UNIQUE INDEX UNIQ_BCD558946CC3BCB5 (cbc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE vt (id INT AUTO_INCREMENT NOT NULL, cbc_id INT DEFAULT NULL, type VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, return_value DOUBLE PRECISION DEFAULT NULL, UNIQUE INDEX UNIQ_6FD479AA6CC3BCB5 (cbc_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE consolidated ADD CONSTRAINT FK_6596A06A6CC3BCB5 FOREIGN KEY (cbc_id) REFERENCES cumulative_benchmark_comparison (id)');
        $this->addSql('ALTER TABLE efa ADD CONSTRAINT FK_B84F4D366CC3BCB5 FOREIGN KEY (cbc_id) REFERENCES cumulative_benchmark_comparison (id)');
        $this->addSql('ALTER TABLE spxtr ADD CONSTRAINT FK_BCD558946CC3BCB5 FOREIGN KEY (cbc_id) REFERENCES cumulative_benchmark_comparison (id)');
        $this->addSql('ALTER TABLE vt ADD CONSTRAINT FK_6FD479AA6CC3BCB5 FOREIGN KEY (cbc_id) REFERENCES cumulative_benchmark_comparison (id)');
        $this->addSql('DROP TABLE pbc_cumulative_benchmark_comparison');
        $this->addSql('DROP TABLE pbc_consolidated');
        $this->addSql('DROP TABLE pbc_efa');
        $this->addSql('DROP TABLE pbc_spxtr');
        $this->addSql('DROP TABLE pbc_vt');
    }
}
