<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200625181243 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pbc_consolidated CHANGE return_value return_value FLOAT');
        $this->addSql('ALTER TABLE pbc_efa CHANGE return_value return_value FLOAT');
        $this->addSql('ALTER TABLE pbc_spxtr CHANGE return_value return_value FLOAT');
        $this->addSql('ALTER TABLE pbc_vt CHANGE return_value return_value FLOAT');
        $this->addSql('ALTER TABLE rmbc_consolidated CHANGE peak_to_valley peak_to_valley VARCHAR(255) DEFAULT NULL, CHANGE recovery recovery VARCHAR(255) DEFAULT NULL, CHANGE positive_periods positive_periods VARCHAR(255) DEFAULT NULL, CHANGE negative_periods negative_periods VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE rmbc_efa CHANGE peak_to_valley peak_to_valley VARCHAR(255) DEFAULT NULL, CHANGE recovery recovery VARCHAR(255) DEFAULT NULL, CHANGE positive_periods positive_periods VARCHAR(255) DEFAULT NULL, CHANGE negative_periods negative_periods VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE rmbc_spxtr CHANGE peak_to_valley peak_to_valley VARCHAR(255) DEFAULT NULL, CHANGE recovery recovery VARCHAR(255) DEFAULT NULL, CHANGE positive_periods positive_periods VARCHAR(255) DEFAULT NULL, CHANGE negative_periods negative_periods VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE rmbc_vt CHANGE peak_to_valley peak_to_valley VARCHAR(255) DEFAULT NULL, CHANGE recovery recovery VARCHAR(255) DEFAULT NULL, CHANGE positive_periods positive_periods VARCHAR(255) DEFAULT NULL, CHANGE negative_periods negative_periods VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pbc_consolidated CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbc_efa CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbc_spxtr CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE pbc_vt CHANGE return_value return_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE rmbc_consolidated CHANGE peak_to_valley peak_to_valley VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE recovery recovery VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE positive_periods positive_periods VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE negative_periods negative_periods VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE rmbc_efa CHANGE peak_to_valley peak_to_valley VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE recovery recovery VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE positive_periods positive_periods VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE negative_periods negative_periods VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE rmbc_spxtr CHANGE peak_to_valley peak_to_valley VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE recovery recovery VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE positive_periods positive_periods VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE negative_periods negative_periods VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE rmbc_vt CHANGE peak_to_valley peak_to_valley VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE recovery recovery VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE positive_periods positive_periods VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE negative_periods negative_periods VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
