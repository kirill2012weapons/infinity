<?php

return [
    'Cash',
    'Consumer Cyclicals',
    'Technology',
    'Healthcare',
    'Industrials',
    'Real Estate',
    'Energy',
    'Unclassified',
    'Financials',
    'Telecomm',
    'Utilities',
    'Basic Materials',
    'Consumer Non-Cyc',
];