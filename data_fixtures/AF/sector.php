<?php

return [
    'Basic Materials',
    'Consumer Cyclicals',
    'Consumer Non-Cyc',
    'Energy',
    'Financials',
    'Healthcare',
    'Industrials',
    'Real Estate',
    'Technology',
    'Telecomm',
    'Utilities',
    'Cash',
    'Unclassified',
];