<?php

return [
    [
        'sector'     => 'Basic Materials',
        'allocation' => '-1.968502473',
        'selection'  => '0.504338701',
        'total'      => '-1.464163772',
    ],
    [
        'sector'     => 'Consumer Cyclicals',
        'allocation' => '-7.940194681',
        'selection'  => '46.892694995',
        'total'      => '38.952500314',
    ],
    [
        'sector'     => 'Consumer Non-Cyc',
        'allocation' => '-0.147397642',
        'selection'  => '0.227269957',
        'total'      => '0.079872315',
    ],
    [
        'sector'     => 'Energy',
        'allocation' => '14.992520032',
        'selection'  => '-28.874457504',
        'total'      => '-13.881937472',
    ],
    [
        'sector'     => 'Financials',
        'allocation' => '17.917236139',
        'selection'  => '-7.826939691',
        'total'      => '10.090296448',
    ],
    [
        'sector'     => 'Healthcare',
        'allocation' => '0.515929406',
        'selection'  => '4.088282515',
        'total'      => '4.604211921',
    ],
    [
        'sector'     => 'Industrials',
        'allocation' => '10.144787201',
        'selection'  => '-4.623204367',
        'total'      => '5.521582833',
    ],
    [
        'sector'     => 'Real Estate',
        'allocation' => '6.651062762',
        'selection'  => '1.791644528',
        'total'      => '8.442707291',
    ],
    [
        'sector'     => 'Technology',
        'allocation' => '2.337899623',
        'selection'  => '6.836333022',
        'total'      => '9.174232646',
    ],
    [
        'sector'     => 'Telecomm',
        'allocation' => '1.991123666',
        'selection'  => '-0.036584639',
        'total'      => '1.954539027',
    ],
    [
        'sector'     => 'Utilities',
        'allocation' => '1.074708054',
        'selection'  => '-1.329721085',
        'total'      => '-0.255013031',
    ],
    [
        'sector'     => 'Cash',
        'allocation' => '0',
        'selection'  => '-0.020606077',
        'total'      => '-0.020606077',
    ],
    [
        'sector'     => 'Unclassified',
        'allocation' => '0',
        'selection'  => '0.32006779',
        'total'      => '0.32006779',
    ],
];