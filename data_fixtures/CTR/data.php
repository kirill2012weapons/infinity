<?php

return [
    [
        'sector'                         => 'Basic Materials',
        'accountContributionToReturn'    => '-2.336153516',
        'bmContributionToReturn'         => '-0.078726196',
        'contributionToReturnDifference' => '-2.25742732',
    ],
    [
        'sector'                         => 'Consumer Cyclicals',
        'accountContributionToReturn'    => '-6.067923076',
        'bmContributionToReturn'         => '0.761666098',
        'contributionToReturnDifference' => '-6.829589174',
    ],
    [
        'sector'                         => 'Consumer Non-Cyc',
        'accountContributionToReturn'    => '1.756784144',
        'bmContributionToReturn'         => '-0.412090298',
        'contributionToReturnDifference' => '2.168874442',
    ],
    [
        'sector'                         => 'Energy',
        'accountContributionToReturn'    => '-14.827058566',
        'bmContributionToReturn'         => '-1.07288292',
        'contributionToReturnDifference' => '-13.754175646',
    ],
    [
        'sector'                         => 'Financials',
        'accountContributionToReturn'    => '2.202256059',
        'bmContributionToReturn'         => '-2.37455707',
        'contributionToReturnDifference' => '4.576813129',
    ],
    [
        'sector'                         => 'Healthcare',
        'accountContributionToReturn'    => '5.790861186',
        'bmContributionToReturn'         => '-0.155999661',
        'contributionToReturnDifference' => '5.946860847',
    ],
    [
        'sector'                         => 'Industrials',
        'accountContributionToReturn'    => '-1.082945636',
        'bmContributionToReturn'         => '-1.335249509',
        'contributionToReturnDifference' => '0.252303872',
    ],
    [
        'sector'                         => 'Real Estate',
        'accountContributionToReturn'    => '2.86577343',
        'bmContributionToReturn'         => '-0.146234111',
        'contributionToReturnDifference' => '3.01200754',
    ],
    [
        'sector'                         => 'Technology',
        'accountContributionToReturn'    => '7.523461841',
        'bmContributionToReturn'         => '2.335616228',
        'contributionToReturnDifference' => '5.187845613',
    ],
    [
        'sector'                         => 'Telecomm',
        'accountContributionToReturn'    => '2.620844681',
        'bmContributionToReturn'         => '0.046360016',
        'contributionToReturnDifference' => '2.574484666',
    ],
    [
        'sector'                         => 'Utilities',
        'accountContributionToReturn'    => '-0.692743536',
        'bmContributionToReturn'         => '-0.37039257',
        'contributionToReturnDifference' => '-0.322350966',
    ],
    [
        'sector'                         => 'Cash',
        'accountContributionToReturn'    => '-0.432106131',
        'bmContributionToReturn'         => '0',
        'contributionToReturnDifference' => '-0.432106131',
    ],
    [
        'sector'                         => 'Unclassified',
        'accountContributionToReturn'    => '0.985829407',
        'bmContributionToReturn'         => '0',
        'contributionToReturnDifference' => '0.985829407',
    ],
];