<?php

return [
    [
        'date'         => 'Jan 2020',
        'SPXTR'        => [
            'return'   => -2.978188976
        ],
        'EFA'          => [
            'return'   => -2.978188976
        ],
        'VT'           => [
            'return'   => -2.978188976
        ],
        'CONSOLIDATED' => [
            'return'   => -2.978188976
        ]
    ],
    [
        'date'         => 'Feb 2020',
        'SPXTR'        => [
            'return'   => -8.231872996
        ],
        'EFA'          => [
            'return'   => -7.765263782
        ],
        'VT'           => [
            'return'   => -7.22438229
        ],
        'CONSOLIDATED' => [
            'return'   => -6.115213892
        ]
    ],
    [
        'date'         => 'Mar 2020',
        'SPXTR'        => [
            'return'   => -12.351353105
        ],
        'EFA'          => [
            'return'   => -14.106683805
        ],
        'VT'           => [
            'return'   => -14.757098812
        ],
        'CONSOLIDATED' => [
            'return'   => 12.121230709
        ]
    ],
    [
        'date'         => 'Apr 2020',
        'SPXTR'        => [
            'return'   => 12.819403325
        ],
        'EFA'          => [
            'return'   => 5.817433595
        ],
        'VT'           => [
            'return'   => 10.366242038
        ],
        'CONSOLIDATED' => [
            'return'   => 3.608842907
        ]
    ],
    [
        'date'         => 'May 2020',
        'SPXTR'        => [
            'return'   => 4.76274585
        ],
        'EFA'          => [
            'return'   => 5.42690472
        ],
        'VT'           => [
            'return'   => 5.208483624
        ],
        'CONSOLIDATED' => [
            'return'   => -7.277264467
        ]
    ],
    [
        'date'         => 'Jun 2020',
        'SPXTR'        => [
            'return'   => 0.828381084
        ],
        'EFA'          => [
            'return'   => 3.198754192
        ],
        'VT'           => [
            'return'   => 2.194185409
        ],
        'CONSOLIDATED' => [
            'return'   => -7.980219136
        ]
    ]
];