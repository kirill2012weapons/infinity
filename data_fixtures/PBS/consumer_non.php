<?php

return [
    'Technology',
    'Consumer Cyclicals',
    'Healthcare',
    'Consumer Non-Cyc',
    'Energy',
    'Industrials',
    'Financials',
    'Real Estate',
    'Telecomm',
    'Basic Materials',
    'Utilities',
    'Unclassified',
];