$(document).ready(function () {

    $('.nav-icon').click(function (e) {
        e.stopImmediatePropagation();
        $(this).toggleClass('open');
        $('.menu-drop').toggleClass('open');
    });

    $('.header .menu-drop .header-list a').click(function() {
        $('.menu-drop').removeClass('open');
        $('.nav-icon').removeClass('open');
    });

    if ($('#particles-js').length > 0) {
        particlesJS.load('particles-js', 'public/build/front/js/particles.json', function() {
            console.log('callback - particles.js config loaded');
        });
    }
});